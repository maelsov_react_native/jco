import { AsyncStorage } from 'react-native'
import { APIRoot } from '../Const'
import { Actions } from 'react-native-router-flux'
import cacheGet from '../libraries/cacheGet'
import cacheSet from '../libraries/cacheSet'
import { toastWarning, toastError } from '../Global';

export default {
    user_name: undefined,
    user_fullname: undefined,
    user_id: undefined,
    isAuth: false,
    token: undefined,
    attemptLogin: (user_name, password) => new Promise(resolve => {
        let formdata = new FormData();
        formdata.append('user_login_name', user_name)
        formdata.append('password', password)

        fetch(APIRoot + 'login', {
            method: 'POST',
            headers: {
                'Accept': 'multipart/form-data',
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
            .then(response => {
                const statusCode = response.status;
                const res = response.json();
                return Promise.all([statusCode, res])
            })
            .then(([statusCode, result]) => {

                console.log(result)
                if (statusCode === 200) {
                    // If login successful
                    const key = result.token
                    console.log('masuk')
                    // Save the key to the local storage
                    AsyncStorage.setItem("token", JSON.stringify({
                        token: result.token,
                        user_id: result.user_id,
                        user_name: result.user_name,
                        user_fullname: result.user_fullname
                    }), () => {
                        this.token = result.token
                        this.user_name = result.user_name
                        this.user_id = result.user_id
                        this.user_fullname = result.user_fullname
                        this.isAuth = true
                        resolve(true)
                    })
                    AsyncStorage.setItem("user_fullname", JSON.stringify({
                        user_fullname: result.user_fullname
                    }), () => {
                        this.user_fullname = result.user_fullname
                        resolve(true)
                    })
                } else {
                    resolve(false)
                }
            })
            .catch((error) => {
                console.log(error.message);
                resolve("connectionError")
            });

    }),
    updateName: (user_full_name) => new Promise(resolve => {
        AsyncStorage.setItem("user_full_name", JSON.stringify({
            user_full_name: user_full_name,
        }), () => {
            this.user_full_name = user_full_name
            resolve(true)
        })
    }),
    getName: () => new Promise(resolve => {
        AsyncStorage.getItem("user_full_name", (error, result) => {
            if (!result) {
                resolve(null)
            }
            else {
                resolve(result)
            }
        })
    }),
    updatePhoto: (user_avatar) => new Promise(resolve => {
        AsyncStorage.setItem("user_avatar", JSON.stringify({
            user_avatar: user_avatar,
        }), () => {
            this.user_avatar = user_avatar
            resolve(true)
        })
    }),
    getPhoto: () => new Promise(resolve => {
        AsyncStorage.getItem("user_avatar", (error, result) => {
            if (!result) {
                resolve(null)
            }
            else {
                resolve(result)
            }
        })
    }),
    validateToken: () => new Promise(resolve => {
        // Check if a token exists in the local storage
        AsyncStorage.getItem("token", (error, result) => {
            if (!result) {
                resolve(null)
            }
            else {
                resolve(result)
            }
        })
    }),
    changePassword: (user_id, old_password, new_password, new_password_confirm, token) => new Promise(resolve => {

        if (user_id !== null && user_id !== undefined) {
            let formdata = new FormData();
            formdata.append('user_id', user_id)
            formdata.append('old_password', old_password)
            formdata.append('new_password', new_password)
            formdata.append('new_password_confirm', new_password_confirm)

            console.log(formdata)

            fetch(APIRoot + 'auth/change_password', {
                method: 'POST',
                headers: {
                    'Accept': 'multipart/form-data',
                    'Content-Type': 'multipart/form-data',
                    'X-Api-Key': token
                },
                body: formdata
            })
                .then(response => {
                    console.log(response)
                    const statusCode = response.status;
                    const res = response.json();
                    return Promise.all([statusCode, res])
                })
                .then(([statusCode, result]) => {

                    if (statusCode === 200) resolve(true)
                    else if (statusCode === 400) toastError('Wrong password!')
                    else resolve(false)
                })
                .catch((error) => {
                    console.log(error.message);
                    resolve("connectionError")
                });
        } else {
            toastWarning('Login first to change the password!')
        }

    }),
    logoutUser: (user_id, token) => new Promise(resolve => {
        let formdata = new FormData();
        formdata.append('user_id', user_id)

        console.log(formdata)

        fetch(APIRoot + 'logout', {
            method: 'POST',
            headers: {
                'Accept': 'multipart/form-data',
                'Content-Type': 'multipart/form-data',
                'X-Api-Key': token
            },
            body: formdata
        })
            .then(response => {
                console.log(response)
                const statusCode = response.status;
                const res = response.json();
                return Promise.all([statusCode, res])
            })
            .then(([statusCode, result]) => {
                if (statusCode === 200) {
                    let keys = ["token", "user_id", "user_full_name", "user_name"]
                    AsyncStorage.multiRemove(keys, error => {
                        resolve(error)
                    })
                } else {
                    resolve(false)
                }
            })
            .catch((error) => {
                console.log(error.message);
                resolve("connectionError")
            });
    }),
    cacheAndRequestToken: (onUpdate, onError = () => { }) => {
        return cacheGet(onUpdate, 'firebase_notification')
    },
    introductionScreenPassed: () => new Promise(resolve => {
        // Save the key to the local storage
        AsyncStorage.setItem("introduction", JSON.stringify({
            attempt: "ok"
        }), () => {
            resolve(true)
        })
    }),
    validateIntroductionScreen: () => new Promise(resolve => {
        // Check if the attempt exists in the local storage
        AsyncStorage.getItem("introduction", (error, result) => {
            if (!result) {
                resolve(null)
            }
            else {
                resolve(result)
            }
        })
    }),
    validateDone: () => new Promise(resolve => {
        // Save the key to the local storage
        AsyncStorage.setItem("allowPush", JSON.stringify({
            attempt: "ok"
        }), () => {
            resolve(true)
        })
    }),
    validatePushNotification: () => new Promise(resolve => {
        // Check if the attempt exists in the local storage
        AsyncStorage.getItem("allowPush", (error, result) => {
            if (!result) {
                resolve(null)
            }
            else {
                resolve(result)
            }
        })
    }),
}