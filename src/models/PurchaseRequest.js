import { APIRoot }  from '../Const'
import cacheGet from '../libraries/cacheGet';
import user from '../models/User'
import requestGet from '../libraries/requestGet';

export default {
    /**
     * Get all purchase request
     */
    cacheAndRequestAll: (onUpdate, onError = () => { toastWarning("No Internet Connection") }) => {
        requestGet(onUpdate, 'purchase_request', APIRoot + "view_pr?status=1")
    },
    /**
     * Get all purchase request search
     */
    cacheAndRequestSearch: (search ,onUpdate, onError = () => { toastWarning("No Internet Connection") }) => {
        console.log(search)
        var url = ''
        if(search !== null) url += '&material_name=' + search
        requestGet(onUpdate, 'purchase_request_search', APIRoot + "search_pr?status=1" + url)
    },
    /**
     * Get all purchase request history
     */
    cacheAndRequestFilterPR: (minDate, maxDate, status, onUpdate, onError = () => { toastWarning("No Internet Connection") }) => {
        var url = ''
        console.log(status)
        if(minDate !== null && maxDate !== null) url += '&from=' + minDate + '&to=' + maxDate
        if(status !== null && status !== "All Status" && status !== "") url +=  '&priority=' + status 
        requestGet(onUpdate, 'purchase_request_filter', APIRoot + "view_pr?status=1" + url)
    },
    /**
     * Get purchase request detail based on ID
     */
    cacheAndRequestDetail: (id, onUpdate, onError = () => { toastWarning("No Internet Connection") }) => {
        user.validateToken().then(result => {
            res = JSON.parse(result)
            requestGet(onUpdate, "purchase_request_detail" + id, APIRoot + 'view_pr_detail?user_login_name='
            + res.user_name +'&pr_id=' + id)
        })
    },
    /**
     * Get all purchase request cache
     */
    cacheGetPR: (onUpdate, onError = () => {}) => {
        cacheGet(onUpdate, 'purchase_request')
    },
    /**
     * Get all purchase request history
     */
    cacheAndRequestHistory: (onUpdate, onError = () => { toastWarning("No Internet Connection") }) => {
        requestGet(onUpdate, 'purchase_request_history', APIRoot + "view_pr?status=0")
    },
    /**
     * Get all purchase request search
     */
    cacheAndReqHistorySearch: (search ,onUpdate, onError = () => { toastWarning("No Internet Connection") }) => {
        console.log(search)
        var url = ''
        if(search !== null) url += '&material_name=' + search
        requestGet(onUpdate, 'purchase_request_history_search', APIRoot + "search_pr?status=0" + url)
    },
    /**
     * Get all purchase request history
     */
    cacheAndRequestFilterHistory: (minDate, maxDate, status, onUpdate, onError = () => { toastWarning("No Internet Connection") }) => {
        var url = ''
        if(minDate !== null && maxDate !== null) url += '&from=' + minDate + '&to=' + maxDate
        if(status !== null && status !== "All Status" && status !== "") url +=  '&priority=' + status 
        requestGet(onUpdate, 'purchase_request_filter_history', APIRoot + "view_pr?status=0" + url)
    },
    /**
     * Get all purchase request history cache
     */
    cacheGetPRHistory: (onUpdate, onError = () => {}) => {
        cacheGet(onUpdate, 'purchase_request_history')
    }
}