import {
    StyleSheet,
    Platform,
    Dimensions
} from 'react-native';
import { Fonts, Metrics, Colors } from './Themes/';

const { width, height } = Dimensions.get('window')

const GlobalStyle = StyleSheet.create({
    greyText: {
        color: "#2E2E2E"
    },
    disabledText: {
        color: "#C8C8C8"
    },
    loginScreenText: {
        color: "#555555", 
        textAlign: "center", 
        fontSize: 16
    },
    titleText: {
        fontSize: 20,
        color: '#363636',
        fontWeight: "300"
    },
    titleTextForm: {
        fontSize: 18,
        textAlign: "center", 
        color: '#363636',
        fontWeight: "300",
        fontWeight: "bold"
    },
    titleTextLeft: {
        fontSize: 16,
        color: '#363636',
        textAlign: "left",
        fontWeight: "300"
    },
    titleTextGreen: {
        fontSize: 16,
        color: '#EEE',
        textAlign: "left",
        fontWeight: "300"
    },
    imageBox: {
        width: (Metrics.WIDTH) * 0.90,
        height: (Metrics.WIDTH) * 0.90,
        alignSelf: 'center',
        marginTop: (Metrics.HEIGHT) * 0.03
      },
    titleTextRight: {
        fontSize: 16,
        color: '#363636',
        textAlign: "right",
        fontWeight: "300"
    },
    nameText: {
        fontSize: 17,
        fontWeight: "bold",
        color: "#555555"
    },
    subtitleText: {
        fontSize: 17,
        color: '#363636'
    },
    subtitleTextService: {
        fontSize: 14,
        color: '#363636'
    },
    contentText: {
        textAlign: "left",
        fontSize: 14,
        color: "#6f6f6f"
    },
    contentCatatan: {
        textAlign: "justify",
        fontSize: 14,
        color: "#6f6f6f",
        width: (Metrics.WIDTH * 0.3)
    },
    containerJustify:{
        flex: 1,
        justifyContent: "center"
    },
    keyboardAvoidingStyle:{
        flex:1, 
        backgroundColor: "#fff",
        height:'100%'
    },
    iconFontSize:{
        fontSize: 18
    },
    titleTextBig: {
        fontSize: 20,
        color: '#444444'
    },
    modalStyle:{
        backgroundColor: "#fff", 
        padding: 20
    },
    buttonDeleteMaterial:{
        elevation: 0 ,
        backgroundColor: "transparent",
        paddingHorizontal: 10
    },
    modalTitle:{
        fontSize: 20,
        textAlign: "center", 
        fontWeight: "bold", 
        color: '#2E2E2E', 
        marginBottom: 10
    },
    modalContent:{
        textAlign: "center", 
        color: '#2E2E2E', 
        marginBottom: 30
    },
    modalYaButton:{
        height: 60,
        backgroundColor: "#21bc62",
        marginRight: 10,
        flex: 0.5
    },
    modalTidakButton:{
        height: 60,
        backgroundColor: "#bc1d15",
        marginLeft: 10,
        flex: 0.5
    },
    buttonModalText:{
        color: "#fff", 
        fontWeight: "bold", 
        fontSize: 16
    },
    modalConnectionErrorTitleText:{
        textAlign: "center", 
        fontWeight: "bold", 
        color: '#2E2E2E', 
        marginBottom: 25
    },
    modalConnectionErrorButton:{
        backgroundColor: "#2f3e69", 
        flex: 0.5
    },
    modalConnectionErrorButtonText:{
        color: "#fff", 
        fontWeight: "bold", 
        fontSize: 16
    },
    connectionNotAvailableTextStyle: { 
        textAlign: "center", 
        fontWeight: "bold", 
        color: '#2E2E2E', 
        marginBottom: 25
    },
    textPicker: {
        color: "#2E2E2E",
        fontSize: 16,
        paddingHorizontal: '5%',
        paddingVertical: '8%'
    },
    containerFlexRow:{
        flexDirection: "row", 
        justifyContent: "center"
    },
    modalButtonText:{
        fontSize: 16, 
        color: "#EEE", 
        fontWeight: "bold", 
        textAlign: "center"
    },
    activityIndicatorTextStyle:{
        color: '#FFF'
    },
    modalButtonCenterContainer:{
        justifyContent: "center"
    },
    buttonGreen:{
        flex: 1, 
        paddingHorizontal: 10,
        borderRadius:5, 
        backgroundColor: "#21bc62"
    },
    buttonModal:{
        flex: 0.5, 
        paddingHorizontal: 10, 
        borderRadius:5,
        backgroundColor: "#21bc62"
    },
    buttonModalPostService:{
        flex: 1, 
        paddingHorizontal: 10, 
        backgroundColor: "#21bc62"
    },
    buttonGreenInCard:{
        backgroundColor: "#21bc62"
    },
    buttonGreenHalf:{
        flex: 0.25, 
        paddingHorizontal: 30, 
        borderRadius:5, 
        backgroundColor: "#21bc62"
    },
    buttonDrawer:{
        flex: 0.25,
        paddingHorizontal: "20%",
        backgroundColor: "#21bc62",
        justifyContent: "center"
    },
    buttonRed:{
        flex: 1, 
        paddingHorizontal: 10, 
        backgroundColor: "#BD222A"
    },
    buttonText: 
    Platform.select({
        ios:{
            fontSize: 14, 
            color: "#EEE", 
            fontWeight: "bold"
        },
        android:{
            fontSize: 18, 
            color: "#EEE", 
            fontWeight: "bold"
        }
    }),
    titleRow:{
        alignItems: "center", 
        justifyContent: "center"
    },
    backgroundImage: {
        flex: 1,
        position:'absolute',
        width:(Metrics.WIDTH),
        height:(Metrics.HEIGHT)* 1.2
    },
    backgroundCenter: {
        width:(Metrics.WIDTH),
        height:(Metrics.HEIGHT),
        justifyContent:'center',
        alignItems:'center',
    },
    whiteBackgroundOverlay: {
        backgroundColor: "rgba(255,255,255,0.8)", 
        margin: "6%",
        paddingBottom: 35
    },
    horizontalLine:{
        marginVertical: 10, 
        borderBottomColor: '#E8E8E8', 
        borderBottomWidth: 1, 
        width: '100%'
    },
    horizontalLineGreen:{
        marginVertical: 10, 
        borderBottomColor: '#21bc62', 
        borderBottomWidth: 1, 
        width: '100%'
    },
    iconStarStyle:{
        fontSize: 18, 
        color: "#FFC600"
    },
    formContainer: {
        flex: 1, 
        alignItems: "center", 
        justifyContent: "center", 
        flexDirection: "row"
    },
    formPickerStyle:{
        width: "100%", 
        borderBottomColor: '#BFBFBF', 
        borderBottomWidth: 1
    },
    pickerStyle:{
        width: "100%",
        borderBottomColor: '#444444',
        borderBottomWidth: 0.5,
        paddingHorizontal: '3%'
    },
    borderFloating: {
        borderBottomColor: '#444444',
        borderBottomWidth: 0.5
    },
    floatingLabelStyle:{
        borderBottomColor: '#BFBFBF', 
        borderBottomWidth: 1
    },
    collapseStyle:{
        borderWidth: 1, 
        borderColor: '#555555', 
        padding: 10,
        marginBottom: 10
    },
    notificationGreenBox:{
        backgroundColor: '#21bc62',
        borderRadius: 50,
        marginLeft: 5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationOrangeBox:{
        backgroundColor: '#F66E25',
        borderRadius: 50,
        marginLeft: 5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationRedBox:{
        backgroundColor: '#BD222A',
        borderRadius: 50,
        marginLeft: 5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationGreyBox:{
        backgroundColor: '#555555',
        borderRadius: 50,
        marginLeft: 5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
        notificationGreenBox:{
        backgroundColor: '#21bc62',
        borderRadius: 50,
        marginLeft: 5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationOrangeBox:{
        backgroundColor: '#F66E25',
        borderRadius: 50,
        marginLeft: 5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationRedBox:{
        backgroundColor: '#BD222A',
        borderRadius: 50,
        marginLeft: 5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationGreyBox:{
        backgroundColor: '#555555',
        borderRadius: 50,
        marginLeft: 5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    // NOTIFICATION DEPENDS ON CONTENT
    notificationGreenBoxFull:{
        backgroundColor: '#21bc62',
        paddingVertical: 0,
        paddingHorizontal: 5,
        borderRadius: 50,
        marginLeft: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationOrangeBoxFull:{
        backgroundColor: '#F66E25',
        paddingVertical: 0,
        paddingHorizontal: 5,
        borderRadius: 50,
        marginLeft: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationRedBoxFull:{
        backgroundColor: '#BD222A',
        paddingVertical: 0,
        paddingHorizontal: 5,
        borderRadius: 50,
        marginLeft: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationGreyBoxFull:{
        backgroundColor: '#555555',
        paddingVertical: 0,
        paddingHorizontal: 5,
        borderRadius: 50,
        marginLeft: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    notificationText:{
        color: "#EEE",
        fontSize: Fonts.moderateScale(13),
        fontWeight: "bold"
    },
    notificationTextSmall:{
        color: "#EEE",
        fontSize: Fonts.moderateScale(11),
        fontWeight: "bold"
    }
});

export default GlobalStyle;
