import {
    StyleSheet,
    Platform,
    Dimensions
} from 'react-native';
import { Fonts, Metrics, Colors } from './Themes/';

const { width, height } = Dimensions.get('window')

const GlobalStyle = StyleSheet.create({
    backgroundCenter: {
        width:(Metrics.WIDTH),
        height:(Metrics.HEIGHT),
        justifyContent:'center',
        alignItems:'center',
    },
    whiteBackgroundOverlay: {
        backgroundColor: "rgba(255,255,255,0.8)", 
        borderRadius: 5,
        margin: "6%",
        paddingBottom: 35
    },
    buttonText: {
        fontSize: 18,
        color: "#EEE",
        fontWeight: "bold"
    },
    buttonMaster: {
        color: '#CC8400',    
    },
    loginScreenText: {
        color: "#555555", 
        textAlign: "center", 
        fontSize: 16
    }, 
    loginScreenTextAwal: {
        color: "#555555", 
        textAlign: "center", 
        fontSize: 20
    },
    borderFloating: {
        borderBottomColor: '#444444',
        borderBottomWidth: 0.5
    },
    greyText: {
        color: "#2E2E2E"
    },
    headerBox:{
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    titleText: {
        fontSize: 18,
        color: '#2E2E2E',
        fontWeight: "bold"
    },
    titleRow:{
        flexDirection:'row', 
        marginBottom: 10 
    },
    subtitleText:{
        color: 'rgba(46, 46, 46, 0.9)'
    },
    contentText: {
        textAlign: "left",
        fontSize: 14,
        color: "#6f6f6f"
    },
    subtitleRow:{
        flexDirection:'row'
    },
    statusText:{
        fontSize: 14,
        color: 'rgba(46, 46, 46, 0.9)',
        fontWeight: "bold"
    },
    statusTextAlign:{
        flex: 0.5, 
        flexDirection:'row', 
        justifyContent: "flex-end",
    },
    itemTextAlign:{
        flex: 0.5, 
        flexDirection:'row'
    },
    headingText:{
        color: 'rgba(46, 46, 46, 0.9)',
        fontSize: 14,
        fontWeight: "bold"
    },
    listItemHeadingText:{
        color: 'rgba(46, 46, 46, 0.9)',
        fontSize: 14,
        fontWeight: "bold",
        marginTop: 20, 
        marginBottom: 10, 
        textAlign: "center"
    },
    contentText: {
        color: 'rgba(46, 46, 46, 0.9)',
        // textAlign: "justify"
    },
    tabBarUnderline:{
        backgroundColor: "#543019"
    },
    tabUnderline:{
        backgroundColor: '#fff'
    },
    tabText:{
        color: 'rgba(46, 46, 46, 0.9)'
    },
    tabActive:{
        backgroundColor: '#fff'
    },
    tabTextActive:{
        color: 'rgba(46, 46, 46, 0.9)'
    },
    footerTab:{
        backgroundColor: "#F1EFF4", 
        flexDirection: "row", 
        justifyContent: 'center'
    },
    signatureButton:{
        flex: 0.3, 
        borderWidth: 2, 
        borderColor: "#23C6B6", 
        backgroundColor:'rgba(35, 198, 182, 0.07)', 
        minHeight: 40
    },
    signatureButtonText:{
        color: "#23C6B6", 
        fontWeight: "bold", 
        fontSize: 14
    },
    rejectButton:{
        flex: 0.3, 
        borderWidth: 2, 
        borderColor: "#E98800", 
        backgroundColor:'rgba(233, 136, 0, 0.07)', 
        minHeight: 40
    },
    rejectButtonText:{
        color: "#E98800", 
        fontWeight: "bold", 
        fontSize: 14
    },
    reviseButton:{
        flex: 0.3, 
        borderWidth: 2, 
        borderColor: "#FFAC00", 
        backgroundColor:'rgba(255, 172, 0, 0.07)', 
        minHeight: 40
    },
    reviseButtonText:{
        color: "#FFAC00", 
        fontWeight: "bold", 
        fontSize: 14
    },
    notSentButton:{
        flex: 0.3, 
        borderWidth: 2, 
        borderColor: "#C52121", 
        backgroundColor:'rgba(197, 33, 33, 0.07)', 
        minHeight: 40,
        borderRadius: 5
    },
    notSentButtonText:{
        color: "#C52121", 
        fontWeight: "bold", 
        fontSize: 14
    },
    photoButton:{
        flex: 0.3, 
        borderWidth: 2, 
        borderColor: "#FFAC00", 
        backgroundColor:'rgba(0, 171, 255, 0.07)', 
        minHeight: 40
    },
    photoButtonText:{
        color: "#FFAC00", 
        fontWeight: "bold", 
        fontSize: 14
    },
    approveButton:{
        flex: 0.3, 
        borderWidth: 2, 
        borderColor: "#37C521", 
        backgroundColor:'rgba(55, 197, 33, 0.1)', 
        minHeight: 40,
        borderRadius: 5
    },
    titleTextHeader:{
        fontSize: 20,
        color: '#6a1f08',
        fontWeight: "bold"
    },
    approveDisableButton:{
        flex: 0.3, 
        borderWidth: 2, 
        borderColor: "#9D9D9D", 
        backgroundColor:'rgba(157, 157, 157, 0.1)', 
        minHeight: 40,
        borderRadius: 5
    },
    approveButtonText:{
        color: "#37C521", 
        fontWeight: "bold", 
        fontSize: 14
    },
    approveDisableButtonText:{
        color: "#9D9D9D", 
        fontWeight: "bold", 
        fontSize: 14
    },
    angleRightStyle:{
        fontSize: 20, 
        color:"rgba(46, 46, 46, 0.5)"
    },
    landscapeListStyle:{
        borderWidth:1, 
        borderColor:"rgba(46, 46, 46, 0.3)"
    },
    tabs:{
        backgroundColor: "#543019",
    },
    whiteColor:{
        alignSelf:'center',
        color: "#fff",
        fontSize: 28
    },
    pickerBorder:{
        borderWidth: 1,
        borderColor: "#d6d6d6"
    },
    spaceTop:{
        marginTop: 10
    },
    modalStyle:{
        backgroundColor: "#fff", 
        padding: 20, 
        borderRadius: 10
    },
    modalTitle:{
        textAlign: "center", 
        fontWeight: "bold", 
        color: '#2E2E2E', 
        marginBottom: 25
    },
    modalContent:{
        textAlign: "center", 
        color: '#2E2E2E', 
        marginBottom: 10
    },
    yesButton:{
        marginRight: 10, 
        borderRadius: 5,
        backgroundColor:'#31B057', 
        flex: 0.5
    },
    noButton:{
        marginLeft: 10, 
        borderRadius: 5,
        backgroundColor: "#E52A34", 
        flex: 0.5
    },
    filterButton:{
        marginLeft: 10, 
        backgroundColor:'#0F752E', 
        borderRadius: 5,
        flex: 0.5
    },
    clearButton:{
        marginRight: 10, 
        backgroundColor: "#E52A34", 
        borderRadius: 5,
        flex: 0.5
    },
    buttonText:{
        color: "#fff", 
        borderRadius:5,
        fontWeight: "bold", 
        fontSize: 14
    },
    tabBarUnderlineRed:{
        backgroundColor: "#fff"
    },
    tabUnderlineRed:{
        backgroundColor: '#ff9c04'
    },
    tabTextRed:{
        color: '#fff'
    },
    tabActiveRed:{
        backgroundColor: '#ff9c04'
    },
    tabTextActiveRed:{
        color: '#fff'
    }
});

export default GlobalStyle;