import React, { Component } from 'react';
import RootContainer from './containers/RootContainer'
import { Root } from 'native-base';

export default class App extends Component {
	render() {
		return (
			<Root>
				<RootContainer />
			</Root>
		);
	}
}
