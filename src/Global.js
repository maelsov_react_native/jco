import cacheSet from './libraries/cacheSet'
import { Toast } from 'native-base'
import user from './models/User'
import currencyFormatter from 'currency-formatter'

export function getTextStyle(status){
    if(status == "Low"){
        return {
            fontWeight: "bold", color:'#158B27', fontSize: 15
        }
    }
    if(status == "High"){
        return {
            fontWeight: "bold", color:'#AD2626', fontSize: 15
        }
    }
    if(status == "Medium"){
        return {
            fontWeight: "bold", color:'#BF7F24', fontSize: 15
        }
    }
    else{
        return {
            fontWeight: "bold", color:'rgba(46, 46, 46, 0.9)', fontSize: 15
        }
    } 
}

export function tokenGet(token) {
    return user.cacheAndRequestToken(({ data, updatedAt, isCache }) => {
        if (data !== token && token !== null) {
            cacheSet("firebase_notification", token)
            console.log("cacheSet token")
        } 
    }).then((response) => {
        let res = null
        // console.log(JSON.parse(response).data)
        if (response === null) {
            cacheSet("firebase_notification", token) 
            console.log("cacheSet token")
            res = { data : token }
        }
        else res = JSON.parse(response)
        
        return Promise.all([res.data])
    })
}

export function popIfExists() {
    console.log("back button")
    // if (navigator.getCurrentIndex() > 0) {
    //   navigator.pop()
    //   return true // do not exit app
    // } else {
    //   return false // exit app
    // }
}

export function toastWarning(msg) { 
    Toast.show({
        text: msg,
        duration: 2000,
        position: 'top',
        buttonText: 'OK',
        type: 'warning'
    })
}

export function toastSuccess(msg) { 
    Toast.show({
        text: msg,
        duration: 2000,
        position: 'top',
        buttonText: 'OK',
        type: 'success'
    })
}

export function toastError(msg) { 
    Toast.show({
        text: msg,
        duration: 2000,
        position: 'top',
        buttonText: 'OK',
        type: 'danger'
    })
}

export function getMimeType(type){
    switch (type) {
        case "doc" :
            return "application/msword";
        case "docx" :
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        case "ppt":
            return "application/vnd.ms-powerpoint";
        case "pptx":
            return "application/vnd.openxmlformats-officedocument.presentationml.presentation"
        case "xls" :
            return "application/vnd.ms-excel";
        case "xlsx" :
            return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        case "pdf" :
            return "application/pdf";
        case "png" :
            return "image/png";
        case "bmp" :
            return "application/x-MS-bmp";
        case "gif" :
            return "image/gif";
        case "jpg" :
            return "image/jpeg";
        case "jpeg" :
            return "image/jpeg";
        case "avi" :
            return "video/x-msvideo";
        case "aac" :
            return "audio/x-aac";
        case "mp3" :
            return "audio/mpeg";
        case "mp4" :
            return "video/mp4";
        case "apk" :
            return "application/vnd.Android.package-archive";
        case "txt" :
        case "log" :
        case "h" :
        case "cpp" :
        case "js" :
        case "html" :
            return "text/plain";
        default:
            return "*/*";
    }
}

export function ifNull(data){
    return data == null || data == '' || data == '0000-00-00' ? '-' : data 
}

export function replaceString(symbol, replace, myString){
    return myString.split(symbol).join(replace);
}

export function _totalPriceSum(qty, price){
    var total = 0
    total = qty * price

    return total
}

export function _totalPriceConvert(qty, price, rate){
    var total = 0
    if (rate !== '0') total = qty * price * rate
    else total = qty * price

    return currencyFormatter.format(total, { code: 'IDR' });
}

export function _formatterCurrency(price, rate){
    if (rate !== '0'){
         return currencyFormatter.format(Number(price), { code: 'USD' });
    }
    else{
        return currencyFormatter.format(Number(price), { code: 'IDR' });
    }
}

export function _idr(value, currency) {
    if (value && currency === 'IDR') return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " "+ currency;    
    if (value && currency !== 'IDR') return parseFloat(value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + " "+ currency;  
}