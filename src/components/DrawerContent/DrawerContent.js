import React, { Component } from 'react';
import {
  Text,
  View
} from 'react-native';
import { Container, Content, List, ListItem, Left, Body, Button, Separator } from 'native-base';
import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';
import delay from '../../libraries/timeoutPromise';
import { toastError, toastWarning, toastSuccess } from '../../Global';
import Icon from 'react-native-vector-icons/FontAwesome';
import user from '../../models/User'
import Modal from 'react-native-modal'
import ActivityIndicator from 'react-native-loading-spinner-overlay';

export default class DrawerContent extends Component {

    constructor(props){
        super(props);
        this.state = {
            data: [],
            isLoading: false
        }
    }

    componentWillReceiveProps = () => {
        user.validateToken().then(result => {
            result = JSON.parse(result)
            this.setState({
                data:result
            })
        })
    }

    handleDOList = () => {
        this.context.drawer.close()
        Actions.master({user_id: this.state.data.user_id})
    }

    handleHistory = () => {
        this.context.drawer.close()
        Actions.history({user_id: this.state.data.user_id, user_name: this.state.data.user_name})
    }

    handleChangePass = () => {
        this.context.drawer.close()
        Actions.changepass({user_id: this.state.data.user_id})
    }

    handleLogout = () => {
        this.setState({
            isLoading: true
        })
		this.context.drawer.close()
		// this.sendFirebaseToken(this.state.data.user_id, '', this.state.data.token)
		user.logoutUser(this.state.data.user_id, this.state.data.token).then(result => {
			if (!result) {
				delay(400).then(() => {
                    toastSuccess("Logged out success!")
                    this.setState({
                        isLoading: false
                    })
					Actions.login()
				})
			}
		})

	}
    _showModal = () => this.setState({ isModalVisible: true })
    _hideModal = () => this.setState({ isModalVisible: false })

    render () {
        console.log(this.state.data)
        var content = null;
        if (this.state.data !== null) {
            content =  <Body style={{paddingLeft:10}}>
                            <Text style={{color: "#FFF"}}>{this.state.data.user_fullname }</Text>
                            <Text style={{color: "#FFF"}} note>{this.state.data.user_name}</Text>
                        </Body>
        }

        return (
            <Container style={{backgroundColor: "#fff"}}>
                <ActivityIndicator size={"large"} overlayColor={"rgba(0, 0, 0, 0.5)"} animation={"fade"} visible={this.state.isLoading} textContent={"Processing..."} textStyle={{ color: '#FFF' }} />
                <Content>
                <List style={{backgroundColor: "#ff9c04"}}>
                    <ListItem style={{backgroundColor: "#ff9c04"}}>
                    {content}
                    </ListItem>
                </List>
                <List>
                <Separator bordered>
                    <Text>MENU</Text>
                </Separator>
                <ListItem icon onPress={this.handleDOList}> 
                    <Left style={{flex: 0.1}}>
                    <Icon name="tasks" style={{fontSize: 18}}/>
                    </Left>
                    <Body style={{flex: 0.9}}>
                    <Text>Task</Text>
                    </Body>
                </ListItem>
                <ListItem icon onPress={this.handleHistory}>  
                    <Left style={{flex: 0.1}}>
                    <Icon name="history" style={{fontSize: 18}}/>
                    </Left>
                    <Body style={{flex: 0.9}}>
                    <Text>History</Text>
                    </Body>
                </ListItem>
                <ListItem icon onPress={this.handleChangePass}>  
                    <Left style={{flex: 0.1}}>
                    <Icon name="key" style={{fontSize: 18}}/>
                    </Left>
                    <Body style={{flex: 0.9}}>
                    <Text>Change Password</Text>
                    </Body>
                </ListItem>
                <ListItem icon onPress={this.handleLogout}> 
                    <Left style={{flex: 0.1}}>
                    <Icon name="power-off" style={{fontSize: 18}}/>
                    </Left>
                    <Body style={{flex: 0.9}}>
                    <Text>Sign Out</Text>
                    </Body>
                </ListItem> 
                </List>
                </Content>
                <Modal isVisible={this.state.isModalVisible} onBackButtonPress={this._hideModal} onBackdropPress={this._hideModal} backdropOpacity={0.5}>
                    <View style={{ backgroundColor: "#fff", padding: 20, borderRadius: 10 }}>
                    <Text style={{ textAlign: "center", fontWeight: "bold", color: '#2E2E2E', marginBottom: 25}}>Connection is not available.</Text>
                    <View style={{flexDirection: "row", justifyContent: "center"}}>
                        <Button full onPress={this._hideModal} style={{ backgroundColor: "#2f3e69", flex: 0.5 }}>
                            <Text style={{color: "#fff", fontWeight: "bold", fontSize: 16}}>OK</Text>
                        </Button>
                    </View>
                    </View>
                </Modal>
            </Container>
        )
    }
}

DrawerContent.contextTypes = {
    drawer: PropTypes.object
}