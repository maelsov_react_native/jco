import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    StatusBar
} from 'react-native';
import { Header, Left, Body, Right, Button, View, Text } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import GlobalStyle from '../GlobalStyle';
import user from '../models/User'
import { Actions } from 'react-native-router-flux';

export default class HeaderMenuComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }
    onValueChangeStatus(value) {
        this.setState({
            status: value
        })
    }
    handleSearch = () => {
        Actions.search({user_id: this.state.data.user_id})
    }
    handleSearchHistory = () => {
        Actions.searchHistory({user_id: this.state.data.user_id})
    }
    render() {
        var headerLeft, headerBody, headerRight,  modalContent;
        //Master Detail
        if (this.props.screen === 'masterDetail') {
            headerLeft = <Left style={{ flexDirection: "row", flex: 1, justifyContent: 'flex-start' }}>
                <Button onPress={() => this.context.drawer.open()} transparent>
                    <Icon name="bars" style={{ color: "#6a1f08", fontSize: 20 }} />
                </Button>
                <Button style={{ paddingBottom: 8, flex: 0.5 }}
                    onPress={() => {
                        setTimeout(() => { Actions.refresh({ refresh: false }) }, 500); Actions.pop();
                    }}
                    transparent>
                    <Icon name="angle-left" style={{ color: "#6a1f08", fontSize: 28 }} />
                </Button>
            </Left>
            headerBody = <Body style={{ justifyContent: 'center', flex: 1 }}>
                <Text style={[GlobalStyle.titleTextHeader, { textAlign: "center" }]}>PR Detail</Text>
            </Body>
            headerRight = <Right style={{ flex: 1 }}></Right>
        }
        //Histroy Screen 
        else if (this.props.screen === 'historyScreen') {
            headerLeft = <Left style={{ flexDirection: "row", flex: 1, justifyContent: 'flex-start' }}>
                <Button onPress={() => this.context.drawer.open()} transparent>
                    <Icon name="bars" style={{ color: "#6a1f08", fontSize: 20 }} />
                </Button>
                <Button style={{ paddingBottom: 8, flex: 0.5 }}
                    onPress={() => {
                        setTimeout(() => { Actions.refresh({ refresh: false }) }, 500); Actions.pop();
                    }}
                    transparent>
                    <Icon name="angle-left" style={{ color: "#6a1f08", fontSize: 28 }} />
                </Button>
            </Left>
            headerBody = <Body style={{ justifyContent: 'center', flex: 1 }}>
                <Text style={[GlobalStyle.titleTextHeader, { textAlign: "center" }]}>History</Text>
            </Body>
            headerRight = <Right style={{ flex: 1 }}>
                <Button transparent onPress={this.handleFilter}>
                    <Icon name="filter" style={{ color: "#6a1f08", fontSize: 20 }} />
                </Button>
                <Button transparent onPress={this.handleSearchHistory}>
                    <Icon name="search" style={{ color: "#6a1f08", fontSize: 20 }} />
                </Button>
            </Right>
        }
        //Detail History Screen
        else if (this.props.screen === 'detailHistoryScreen') {
            headerLeft = <Left style={{ flexDirection: "row", flex: 1, justifyContent: 'flex-start' }}>
                <Button onPress={() => this.context.drawer.open()} transparent>
                    <Icon name="bars" style={{ color: "#6a1f08", fontSize: 20 }} />
                </Button>
                <Button style={{ paddingBottom: 8, flex: 0.5 }}
                    onPress={() => {
                        setTimeout(() => { Actions.refresh({ refresh: false }) }, 500); Actions.pop();
                    }}
                    transparent>
                    <Icon name="angle-left" style={{ color: "#6a1f08", fontSize: 28 }} />
                </Button>
            </Left>
            headerBody = <Body style={{ justifyContent: 'center', flex: 1 }}>
                <Text style={[GlobalStyle.titleTextHeader, { textAlign: "center" }]}>Detail History</Text>
            </Body>
            headerRight = <Right style={{ flex: 1 }}></Right>
        }
        //Item Master Detail
        else if (this.props.screen === 'itemDetailMaster') {
            headerLeft = <Left style={{ flexDirection: "row", flex: 1, justifyContent: 'flex-start' }}>
                <Button onPress={() => this.context.drawer.open()} transparent>
                    <Icon name="bars" style={{ color: "#6a1f08", fontSize: 20 }} />
                </Button>
                <Button style={{ paddingBottom: 8, flex: 0.5 }}
                    onPress={() => {
                        setTimeout(() => { Actions.refresh({ refresh: false }) }, 500); Actions.pop();
                    }}
                    transparent>
                    <Icon name="angle-left" style={{ color: "#6a1f08", fontSize: 28 }} />
                </Button>
            </Left>
            headerBody = <Body style={{ justifyContent: 'center', flex: 1 }}>
                <Text style={[GlobalStyle.titleTextHeader, { textAlign: "center" }]}>Item Detail</Text>
            </Body>
            headerRight = <Right style={{ flex: 1 }}></Right>
        }
        //Change Password
        else if (this.props.screen === 'changePassword') {
            headerLeft = <Left style={{ flexDirection: "row", flex: 1, justifyContent: 'flex-start' }}>
                <Button onPress={() => this.context.drawer.open()} transparent>
                    <Icon name="bars" style={{ color: "#6a1f08", fontSize: 20 }} />
                </Button>
                <Button style={{ paddingBottom: 8, flex: 0.5 }}
                    onPress={() => {
                        setTimeout(() => { Actions.refresh({ refresh: false }) }, 500); Actions.pop();
                    }}
                    transparent>
                    <Icon name="angle-left" style={{ color: "#6a1f08", fontSize: 28 }} />
                </Button>
            </Left>
            headerBody = <Body style={{ justifyContent: 'center', flex: 1 }}>
                <Text style={[GlobalStyle.titleTextHeader, { textAlign: "center" }]}>Change Password</Text>
            </Body>
            headerRight = <Right style={{ flex: 1 }}></Right>
        }

        return (
            <View>
                <StatusBar
                    backgroundColor="#6a1f08"
                    barStyle="light-content"
                />
                <Header iosBarStyle="light-content" style={{ backgroundColor: "#ff9c04", elevation: 0 }}>
                    {headerLeft}
                    {headerBody}
                    {headerRight}
                </Header>
            </View>
        )
    }
}

HeaderMenuComponent.contextTypes = {
    drawer: PropTypes.object
}