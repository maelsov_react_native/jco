import { AsyncStorage } from 'react-native'
import user from '../models/User'
import {toastWarning} from '../Global'
export default (update, storageId, APISource, onError = () => {}) => {

    user.validateToken().then(result => {
        res = JSON.parse(result)

        url = ''
        if (APISource.indexOf('?') > -1)
        {
            res !== null ? url = "&user_id=" + res.user_id : null
        }
        else
        {
            res !== null ? url = "?user_id=" + res.user_id : null
        }

        // Request the latest data from the server, if successful, update the view and update the cache
        return fetch(APISource + url, {
            method: 'GET', 
            headers: {
                'X-Api-Key': res !== null ? res.user_token : ''
            }
        })
        .then((response) => {
            console.log(response)
            const statusCode = response.status;
            const res = response.status > 200 ? [] : response.json()
            // const res = response.json()
            return Promise.all([statusCode, res])
        })
        .then(([statusCode, result]) => {
            if(statusCode == 200){
                update({
                    data: result,
                    updatedAt: new Date(),
                    isCache: false })
                    
                AsyncStorage.setItem(storageId, JSON.stringify({
                    data: result,
                    updatedAt: new Date()
                }))
            }
            else if(statusCode == 400 || statusCode == 404){
                update({
                    data: result,
                    updatedAt: new Date(),
                    isCache: false })
            }else if(statusCode == 403){
                user.logoutUser(res.user_id, res.user_token).then(result => {
                    if(!result){
                        toastWarning("You has been logged out! Another user login in another device!")
                    }
                })
                resolve(false)
            }else{
                toastWarning("No Internet Connection")
                onError()
            }
        })
        .catch(onError)
        
    })
}