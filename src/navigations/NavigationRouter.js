import React, { Component } from 'react'
import { Scene, Router, Modal, Actions } from 'react-native-router-flux'
import NavigationDrawer from './NavigationDrawer'

// screens identified by the router
import LoginScreen from '../containers/LoginScreen'
import SplashScreen from '../containers/SplashScreen'
import MasterScreen from '../containers/MasterScreen'
import MasterDetail from '../containers/MasterDetail'
import HistoryScreen from '../containers/HistoryScreen'
import ItemMasterDetail from '../containers/ItemMasterDetail'
import ItemHistoryDetail from '../containers/ItemHistoryDetail'
import HistoryDetail from '../containers/HistoryDetail'
import ResetPassword from '../containers/ResetPassword'
import SearchScreen from '../containers/SearchScreenMaster'
import SearchScreenHistory from '../containers/SearchScreenHistory'
import ChangePasswordScreen from '../containers/ChangePasswordScreen'

class NavigationRouter extends Component {
    render() {
        return (
            <Router>
                <Scene key='root'>
                    <Scene initial key='splash' component={SplashScreen} hideNavBar={true} />
                    <Scene key='login' component={LoginScreen} hideNavBar={true} type='reset' />
                    <Scene key='resetPassword' component={ResetPassword} hideNavBar={true} />
                    <Scene key='drawer' component={NavigationDrawer} hideNavBar={true} open={false} type='reset' >
                        <Scene initial key='master' component={MasterScreen} hideNavBar={true} type='reset' />
                        <Scene key='itemMasterDetail' component={ItemMasterDetail} hideNavBar={true} />
                        <Scene key='itemHistoryDetail' component={ItemHistoryDetail} hideNavBar={true} />
                        <Scene key='masterDetail' component={MasterDetail} hideNavBar={true} />
                        <Scene key='search' component={SearchScreen} hideNavBar={true} />
                        <Scene key='searchHistory' component={SearchScreenHistory} hideNavBar={true} />
                        <Scene key='historyDetail' component={HistoryDetail} hideNavBar={true} />
                        <Scene key='history' component={HistoryScreen} hideNavBar={true} />
                        <Scene key='changepass' component={ChangePasswordScreen} hideNavBar={true} />
                    </Scene>
                </Scene>
            </Router>
        )
    }
}

export default NavigationRouter

