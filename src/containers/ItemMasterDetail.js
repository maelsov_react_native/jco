import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView, 
    StatusBar
} from 'react-native';
import { Container, List, ListItem, Left, Right, CheckBox, Body, Separator, Button, Footer, FooterTab, Badge } from 'native-base';
import GlobalStyle from '../GlobalStyle';
import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import { _totalPriceSum, ifNull, _idr } from '../Global';
import ActivityIndicator1 from 'react-native-loading-spinner-overlay';
import HeaderMenuComponent from '../components/HeaderMenuComponent';
import {APIRoot} from '../Const'

var checked = []

export default class itemMasterDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataDetailPR: [],
            dataDetailPRHeader: [],
            isLoading: false
        }
    }

    componentDidMount() {
        this.fetchData()
    }

    fetchData = () => {
        this.setState({
            isLoading:true
        })
        fetch(APIRoot + "approval_acs/api/v1/auth/view_pr_detail?user_login_name="+this.props.user_name+"&pr_id="+this.props.pr_id+"8&user_id="+ this.props.user_id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                }
                
            })
            .then((responsejson) => {
                this.setState({
                    dataDetailPR: responsejson.pr_items,
                    dataDetailPRHeader: responsejson.pr_header,
                    isLoading: false
                })
            })
            .catch((error) => {
                this.setState({
                    status: "",
                    modalConnectionError: true,
                    isModalVisible: true,
                    isLoading: false
                })
                console.log(error)
            })
    }
    render() {
        console.log(this.props.user_name + this.props.pr_id+ this.props.user_id)
        return (
            <Container>
                <HeaderMenuComponent screen={'itemDetailMaster'} />
                <StatusBar
                    backgroundColor="#6a1f08"
                    barStyle="light-content"
                />
                <ActivityIndicator1 size={"large"} overlayColor={"rgba(0, 0, 0, 0.5)"} animation={"fade"} visible={this.state.isLoading} textContent={"Processing..."} textStyle={{ color: '#FFF' }} />
                <View style={GlobalStyle.headerBox}>
                    <View style={GlobalStyle.titleRow}>
                        <View>
                            <Text style={GlobalStyle.titleText}>{this.props.pr_number}</Text>
                        </View>
                        {this.props.pr_priority === 'Medium' ?
                            <Right>
                                <View>
                                    <Badge warning>
                                        <Text style={[GlobalStyle.buttonText, { paddingTop: '1%' , paddingRight:'2%', paddingLeft: '2%'}]}>Medium</Text>
                                    </Badge>
                                </View>
                            </Right> :
                            this.props.pr_priority === "High" ?
                                <Right>
                                    <View>
                                        <Badge danger>
                                            <Text style={[GlobalStyle.buttonText, { paddingTop: '1%', paddingRight: '10%', paddingLeft: '10%' }]}>High</Text>
                                        </Badge>
                                    </View>
                                </Right> :
                                this.props.pr_priority === 'Low' ?
                                    <Right>
                                        <View>
                                            <Badge style={{ backgroundColor: '#808000' }}>
                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '1%', paddingRight: '10%', paddingLeft: '10%' }]}>Low</Text>
                                            </Badge>
                                        </View>
                                    </Right> : null
                        }
                    </View>
                    <View style={GlobalStyle.subtitleRow}>
                        <View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[GlobalStyle.subtitleText, { fontWeight: 'bold' }]}>Created by : </Text>
                                <Text style={GlobalStyle.subtitleText}>{this.props.user_name}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={GlobalStyle.subtitleRow}>
                        <View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[GlobalStyle.subtitleText, { fontWeight: 'bold' }]}>Created on : </Text>
                                <Text style={GlobalStyle.subtitleText}>{this.props.created}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <ScrollView>
                    <List>
                        <ListItem key={this.props.pr_item_id}>
                            <Body>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Account Assignment : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_item_account_assignment}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Asset Number : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_number}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Order Number : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_item_account_assignment_number}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Item Name : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_item_material_name}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Item Quantity : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_item_qty}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Item Price : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_item_price}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Currency Rate : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_item_currency_rate}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Delivery Date : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_item_delivery_date}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Cost Center : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.cost_center_name}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>GL Account : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.gl_account_desc}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Vendor Name : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_item_vendor_name}{"\n"}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 0.5 }}>
                                    <Text style={GlobalStyle.headingText}>Vendor Address : </Text>
                                    <Text style={GlobalStyle.contentText}>{this.props.pr_item_vendor_address}{"\n"}</Text>
                                </View>
                                {/* <View style={{ flexDirection: 'row', flex: 0.3 }}>
                                    <Text style={GlobalStyle.headingText}>Attechment : </Text>
                                    <Text style={GlobalStyle.contentText}>{"\n"}</Text>
                                </View> */}

                                {/* <View style={{ justifyContent: "center", alignItems: 'center' }}>
                                    <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 20 }}>
                                        <Button onPress={() => this.handleDownload()} full style={{ backgroundColor: "#ff9c04", flex: 0.5 }}>
                                            <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 14 }}><Icon name="download" style={{ fontSize: 16, color: "#fff" }} /> Download File</Text>
                                        </Button>
                                    </View>
                                </View> */}
                            </Body>
                        </ListItem>
                    </List>
                </ScrollView>
            </Container>
        )
    }
}
itemMasterDetail.contextTypes = {
    drawer: PropTypes.object
}
