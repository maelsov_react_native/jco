import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';
import GlobalStyle from '../GlobalStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import { Container, Content, Card, CardItem, Form, Item, Input, Button} from 'native-base';
import { _idr } from '../Global';
import HeaderMenuComponent from '../components/HeaderMenuComponent';
import { APIRoot } from '../Const'
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import { toastError, toastSuccess, toastWarning } from '../Global';
import ActivityIndicator from 'react-native-loading-spinner-overlay';

export default class ChangePasswordScreen extends Component {

    constructor(props){
        super(props);
        this.state = {
            isModalVisible: false,
            hidePassOld: true,
            hidePassNew: true,
            hidePassRetype: true,
            old_password: null,
            new_password: null,
            new_password_confirm: null,
            isLoading: false
        }
    }
    handleFormChange = (field, value) => {
        console.log(field +"="+ value)
        this.setState({
            [field] : value,
            status : ""
        })
    }
    _showPassword = (field) => {
        this.state[field] === true ? this.setState({ [field]: false }) : this.setState({ [field]: true })
    }
    handleInputChange = (field) => (value) => this.handleFormChange(field, value)
    handleSubmit = () => {
        this.setState({
            isModalVisible: false,
            modalSubmit: false,
        })
        if (this.state.old_password == null) {
            this.setState({
                isLoading: false
            })
            toastWarning("Please input old password!")
        }
        else if (this.state.new_password == null) {
            this.setState({
                isLoading: false
            })
            toastWarning("Please input new password!")
        }
        else if (this.state.new_password_confirm == null) {
            this.setState({
                isLoading: false
            })
            toastWarning("Please input re-type new password!")
        }
        else if (this.state.new_password != this.state.new_password_confirm) {
            toastWarning('New password is not the same as a repeat password!')
        }
        else if (this.state.old_password === this.state.new_password){
            toastWarning('New password cannot be the same as the old password!')
        }
        else if (this.state.new_password.length < 8) {
            this.setState({
                password: null,
                isLoading: false
            })
            toastWarning("Password is at least 8 characters long!")
        }
        else if (this.state.new_password.length > 20) {
            this.setState({
                password: null,
                isLoading: false
            })
            toastWarning("Password is a maximum of 20 characters!")
        }
        else {
            this.setState({
                isLoading: true
            })
            
            const formdata = new FormData();
            formdata.append('user_id', this.props.user_id)
            formdata.append('old', this.state.old_password)
            formdata.append('new', this.state.new_password)
            formdata.append('new_confirm', this.state.new_password_confirm)

            console.warn(formdata)
            fetch(APIRoot + 'update_password', {
                method: 'POST',
                headers: {
                    'Accept': 'multipart/form-data',
                    'Content-Type': 'multipart/form-data',
                },
                body: formdata
            })
            .then(response => {
                const statusCode = response.status
                // const res = response.text();
                const res = response.json();
                return Promise.all([statusCode, res])
                // return res
            })
            // .then(text => console.log(text))
            .then(([statusCode, result]) => {
                console.log(statusCode)
                if (statusCode == 200) {
                    this.setState({
                        isLoading: false
                    })
                    toastSuccess("Change password success!")
                    Actions.master()
                }

                else {
                    this.setState({
                        isLoading: false
                    })
                    toastError("Your old password is wrong!!")
                }
            })
            .catch((error) => {
                console.log(error.message);
                this.setState({
                    isLoading: false
                })
                toastError("Connection not available!")
            })
        }
    }
	render() {
		return (
			<Container >
                <ActivityIndicator size={"large"} overlayColor={"rgba(0, 0, 0, 0.5)"} animation={"fade"} visible={this.state.isLoading} textContent={"Processing..."} textStyle={{ color: '#FFF' }} />
				<HeaderMenuComponent screen={'changePassword'}/>
                <StatusBar
                    backgroundColor="#6a1f08"
                    barStyle="light-content"
                />
                <Content style={{ padding : 10}}>
                <Card style={{ padding: 10, borderRadius: 5}}>
                    <CardItem header bordered>
                        <IconAwesome name="key" style={{fontSize: 18, color: '#000'}}/>
                        <Text style={GlobalStyle.titleText}>  Change Password</Text>
                    </CardItem>
                    <View style={{ marginTop: 20}}>
                        <Form>
                            <Item style={{ borderColor: 'transparent' }}>
                                <Text>Old Password</Text>
                            </Item>
                            <Item>
                                <Input autoCapitalize="none" value={this.state.old_password} secureTextEntry={this.state.hidePassOld} onChangeText={this.handleInputChange("old_password")}/>
                                <Icon active onPress={ () => this._showPassword("hidePassOld")} name={ this.state.hidePassOld === true ? 'ios-eye-off' : 'ios-eye'} style={{fontSize: 20}} />         
                            </Item>
                            <Item style={{ borderColor: 'transparent' }}>
                                <Text>New Password</Text>
                            </Item>
                            <Item> 
                                <Input autoCapitalize="none" value={this.state.new_password} secureTextEntry={this.state.hidePassNew} onChangeText={this.handleInputChange("new_password")}/>
                                <Icon active onPress={ () => this._showPassword("hidePassNew")} name={ this.state.hidePassNew === true ? 'ios-eye-off' : 'ios-eye'} style={{fontSize: 20}} />         
                            </Item>
                            <Item style={{ borderColor: 'transparent' }}>
                                <Text>Re-type New Password</Text>
                            </Item>
                            <Item> 
                                <Input autoCapitalize="none" value={this.state.new_password_confirm} secureTextEntry={this.state.hidePassRetype} onChangeText={this.handleInputChange("new_password_confirm")}/>
                                <Icon active onPress={ () => this._showPassword("hidePassRetype")} name={ this.state.hidePassRetype === true ? 'ios-eye-off' : 'ios-eye'} style={{fontSize: 20}} />   
                            </Item>
                            <Button onPress={this.handleSubmit} block style={{marginTop: 20, borderRadius: 6, backgroundColor: "#CC8400"}}>
                                {this.state.status === "fetching" ? <Spinner color = "#fff"/> : <Text style={{fontSize: 18, color: "#EEE", fontWeight: "bold"}}>Submit</Text>}
                            </Button>
                        </Form>
                    </View>
                </Card>
            </Content>
			</Container>
		);
	}
}
ChangePasswordScreen.contextTypes = {
    drawer: PropTypes.object
}