import React, { Component } from 'react'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    ImageBackground
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import Modal from 'react-native-modal';
import user from '../models/User'
import { Spinner, Label, Form, Item, Input, Button } from 'native-base';
import delay from '../libraries/timeoutPromise';
import { toastError, toastSuccess, toastWarning } from '../Global';
import GlobalStyle from '../GlobalStyleLogin';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
    imageContainer: {
        height: "20%",
        flex: 0.3,
        alignItems: "center",
        justifyContent: "center"
    },
    image: {
        flex: 1,
        width: "50%",
        height: "auto",
        resizeMode: "contain"
    },
    loginButton: {
        marginTop: 20,
        borderRadius: 6,
        backgroundColor: "#CC8400"
    },
    loginButtonText: {
        fontSize: 18,
        color: "#EEE",
        borderRadius: 6,
        fontWeight: "bold"
    }
});
class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user_name: "",
            password: "",
            modalConnectionError: false,
            isModalVisible: false,
            isLoading: false
        }
    }
    handleLogin = () => {
        if (this.state.user_name == "" && this.state.password == "") {
            toastWarning("Please input username and password!")
        }
        else if (this.state.user_name == "") {
            toastWarning("Please input username!")
        }
        else if (this.state.password == "") {
            toastWarning("Please input password!")
        }
        else {
            this.setState({
                isLoading: true
            })
            user.attemptLogin(this.state.user_name, this.state.password).then(result => {
                if (result == true) {
                    user.validateToken().then(result => {
                        result = JSON.parse(result)
                    })

                    toastSuccess("Login success!")
                    delay(1500).then(() => {
                        Actions.drawer()
                        Actions.master()
                    })
                }
                else if (result === "connectionError") {
                    this.setState({
                        isLoading: false,
                        modalConnectionError: true,
                        isModalVisible: true
                    })
                }
                else {
                    toastError("Incorrect username and password!")
                    this.setState({
                        isLoading: false
                    })
                    Actions.refresh()
                }
            })
        }
    }
    handleFormChange = (field, value) => {
        console.log(field + "=" + value)
        this.setState({
            [field]: value
        })
    }
    handleInputChange = (field) => (value) => this.handleFormChange(field, value)
    handleReset = () => {
        Actions.resetPassword()
    }
    _hideModal = () => this.setState({
        modalConnectionError: false,
        isModalVisible: false,
        isLoading: false
    })
    
    render() {
        var modalContent = <View />;
        if (this.state.modalConnectionError === true) {
            modalContent = <View style={{ backgroundColor: "#fff", padding: 20, borderRadius: 10 }}>
                <Text style={GlobalStyle.connectionNotAvailableTextStyle}>Koneksi internet tidak tersedia.</Text>
                <View style={GlobalStyle.containerFlexRow}>
                    <Button full onPress={this._hideModal} style={GlobalStyle.buttonModal}>
                        <Text style={GlobalStyle.modalButtonText}>OK</Text>
                    </Button>
                </View>
            </View>;
        }
        return (
            <ImageBackground style={GlobalStyle.backgroundImage} source={require('../Images/test.jpeg')}>
                <View style={[styles.imageContainer, { flex: 0, paddingTop: "2%" }]}>
                    <Image resizeMethod="resize" source={require("../Images/jco-logo.png")} style={styles.image} />
                </View>
                <View style={{ paddingTop: '12%' }}></View>
                <View style={[GlobalStyle.backgroundCenter, { paddingBottom: '50%' }]}>
                    <View style={[GlobalStyle.whiteBackgroundOverlay, { width: 300, margin: 0, borderRadius: 6 }]}>
                        <Form style={{ padding: "6%" }}>
                            <View style={{ paddingHorizontal: 10, paddingVertical: 5 }}>
                                <Text style={[GlobalStyle.titleText, { textAlign: "center" }]}>Login </Text>
                            </View>
                            <Item floatingLabel style={GlobalStyle.borderFloating}>
                                <Label style={{ color: "#2E2E2E" }}>Username</Label>
                                <Input
                                    autoCapitalize="none"
                                    style={GlobalStyle.greyText}
                                    value={this.state.user_name}
                                    onChangeText={this.handleInputChange("user_name")} />
                            </Item>
                            <Item floatingLabel style={GlobalStyle.borderFloating}>
                                <Label style={{ color: "#2E2E2E" }}>Password</Label>
                                <Input
                                    autoCapitalize="none"
                                    style={GlobalStyle.greyText}
                                    value={this.state.password}
                                    secureTextEntry
                                    onChangeText={this.handleInputChange("password")} />
                            </Item>
                            <Button onPress={this.handleLogin} block style={styles.loginButton}>
                                {this.state.isLoading ? <Spinner color="#EEE" /> : <Text style={styles.loginButtonText}>Login</Text>}
                            </Button>
                        </Form>
                        <Text style={[GlobalStyle.loginScreenText, { fontWeight: "bold", marginBottom: 15 }]} onPress={this.handleReset}>Forgot Password ? <Icon name="arrow-right" style={{ fontSize: 16 }} /></Text>
                    </View>
                </View>
                <Modal
                    avoidKeyboard={Platform.OS === "ios" ? true : false}
                    isVisible={this.state.isModalVisible}
                    backdropOpacity={0.9}
                    children={modalContent}
                >
                </Modal>
            </ImageBackground>
        );
    }
}

export default LoginScreen
