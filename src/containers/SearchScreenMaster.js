import React, { Component } from 'react';
import {
  StatusBar,
  ScrollView,
} from 'react-native';
import { Container, Header, Item, Input, Icon, Button, Text, Content, Right, View, ListItem, Body, Left, Badge } from 'native-base';
import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';
import IconBack from 'react-native-vector-icons/FontAwesome';
import GlobalStyle from '../GlobalStyle';
import HeaderMenuComponent from '../components/HeaderMenuComponent';
import { APIRoot } from '../Const'
import SearchScreenHistory from './SearchScreenHistory';
import ActivityIndicator1 from 'react-native-loading-spinner-overlay';


export default class SearchScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      search: '',
      search_pr: [],
      isLoading: false
    }
  }

  _handleSearch = () => {
    this.setState({
      search_pr: [],
      isLoading: true
    })
    fetch(APIRoot + "search_pr?" + "user_id=" + this.props.user_id + "&status=1" + "&material_name=" + this.state.search, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        if (response.status === 200) {
          return response.json()
        }
      })
      .then((responsejson) => {
        this.setState({
          search_pr: responsejson,
        })
      })
      .then(() => {
        console.warn('masuk')
        this.setState({
          isLoading: false
        })
      })

      .catch((error) => {
        this.setState({
          status: "",
          modalConnectionError: true,
          isModalVisible: true,
          isLoading: false
        })
        console.log(error)
      })
  }

  HandleDetailMaster = (pr_number, user_name, created, pr_priority, pr_id) => {
    Actions.masterDetail({ pr_number: pr_number, user_name: user_name, created: created, pr_priority: pr_priority, pr_id: pr_id, username: this.props.username, user_id: this.props.user_id })
  }

  handleFormChange = (field, value) => {
    console.log(field + "=" + value)
    this.setState({
      [field]: value,
      status: ""
    })
  }
  handleBack = () => {
    Actions.pop()
  }
  handleInputChange = (field) => (value) => this.handleFormChange(field, value)
  render() {
    console.log(this.state.search_pr)
    return (
      <Container>
        <Header searchBar rounded iosBarStyle="light-content" style={{ backgroundColor: "#ff9c04" }}>
          <StatusBar
            backgroundColor="#6a1f08"
            barStyle="light-content"
          />
          <Button style={{ paddingBottom: 8, flex: 0.1 }}
            onPress={this.handleBack}
            transparent>
            <IconBack name="angle-left" style={{ color: "#6a1f08", fontSize: 28 }} />
          </Button>
          <Item style={{ flex: 0.9 }}>
            <Icon name="ios-search" onPress={this._handleSearch} />
            <Input
              placeholder="Search"
              value={this.state.search}
              keyboardType="default"
              returnKeyType='search'
              autoFocus
              onChangeText={this.handleInputChange('search')}
              onSubmitEditing={this._handleSearch}
            />
          </Item>
        </Header>
        <ActivityIndicator1 size={"large"} overlayColor={"rgba(0, 0, 0, 0.5)"} animation={"fade"} visible={this.state.isLoading} textContent={"Processing..."} textStyle={{ color: '#FFF' }} />
        <ScrollView style={{ paddingBottom: '70%' }}>
          {this.state.search_pr.map((item) => {
            return (
              <ListItem key={item.pr_id} onPress={() => this.HandleDetailMaster(item.pr_number, item.user_name, item.pr_created_time, item.pr_priority, item.pr_id)}>
                <ScrollView>
                  <View style={GlobalStyle.titleRow}>
                    <Left>
                      <Text style={GlobalStyle.titleText}>{item.pr_number}</Text>
                    </Left>
                    {item.pr_priority === "Low" ?
                      <Right>
                        <View style={{ width: '100%', paddingLeft: '50%' }}>
                          <Badge style={{ backgroundColor: '#808000' }} >
                            <Text style={[GlobalStyle.buttonText, { paddingBottom: '3%', paddingLeft: '25%', paddingRight: '25%', textAlign: 'center' }]}>Low</Text>
                          </Badge>
                        </View>
                      </Right> :
                      item.pr_priority === 'Medium' ?
                        <Right>
                          <View style={{ width: '100%', paddingLeft: '50%' }}>
                            <Badge warning >
                              <Text style={[GlobalStyle.buttonText, { paddingBottom: '3%', paddingLeft: '5%', paddingRight: '5%', textAlign: 'center' }]}>Medium</Text>
                            </Badge>
                          </View>
                        </Right> :
                        item.pr_priority === 'High' ?
                          <Right>
                            <View style={{ width: '100%', paddingLeft: '40%' }}>
                              <Badge danger >
                                <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingLeft: '25%', paddingRight: '25%', textAlign: 'center' }]}>High</Text>
                              </Badge>
                            </View>
                          </Right> : null
                    }
                  </View>
                  <Body>
                    <View style={{ flexDirection: 'row' }}>
                      <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Purpose : </Text>
                      <Text note style={[GlobalStyle.subtitleText, { paddingRight: '5%' }]}>{item.pr_purpose}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Created by : </Text>
                      <Text note style={[GlobalStyle.subtitleText, { paddingRight: '5%' }]}>{item.user_name}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Created on : </Text>
                      <Text note style={[GlobalStyle.subtitleText, { paddingRight: '5%' }]}>{item.pr_created_time}</Text>
                    </View>
                  </Body>
                </ScrollView>
              </ListItem>
            )
          }
          )}
          {this.state.search_pr.length === 0 ?
            <View style={[GlobalStyle.titleRow, { justifyContent: "center", paddingTop: '50%' }]}>
              <Text style={GlobalStyle.statusText}>No Purchase Request Found</Text>
            </View> : null
          }
        </ScrollView>
      </Container>
    );
  }
}
SearchScreen.contextTypes = {
  drawer: PropTypes.object
}