import React, { Component } from 'react';
import {
  View,
  Image,
  StatusBar
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import delay from '../libraries/timeoutPromise';
import user from '../models/User'

export default class SplashScreen extends Component {

  componentDidMount() {
    Promise.all([
      delay(1500)
    ])
      .then(() => {
        user.validateToken()
          .then((result) => {
            result = JSON.parse(result)
            if (!result) Actions.login()
            else {
              Actions.drawer()
              Actions.master()
            }
          })
      })
  }

  render() {

    return (
      <View style={{ flex: 1, padding: 20, margin: 0, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
        <StatusBar backgroundColor="#fff" />
        <Image resizeMethod="resize" source={require("../Images/jco-logo.png")} style={{ flex: 0.8, width: "50%", height: "auto", resizeMode: "contain", justifyContent: "center" }} />
      </View>
    );
  }
}