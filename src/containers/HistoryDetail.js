import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    StatusBar,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';
import GlobalStyle from '../GlobalStyle';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Container, List, ListItem, Left, Right, CheckBox, Body, Separator, Content, Footer, FooterTab, Badge, Tab, Tabs } from 'native-base';
import { getTextStyle, ifNull, _idr } from '../Global';
import HeaderMenuComponent from '../components/HeaderMenuComponent';
import { APIRoot } from '../Const'
import ActivityIndicator from 'react-native-loading-spinner-overlay';
import ActivityIndicator1 from 'react-native-loading-spinner-overlay';


export default class HistoryDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataDetailPR: [],
            PR: [],
            dataDetailPRHeader: [],
            dataApproved: [],
            isLoading: false,
            modalStart: false,
            modalCancel: false,
        }
    }
    componentDidMount() {
        this.fetchData()
    }

    fetchData = () => {
        this.setState({
            isLoading: true
        })
        console.log(APIRoot + "view_pr_detail?user_login_name=" + this.props.username + "&pr_id=" + this.props.pr_id + "&user_id=" + this.props.user_id)
        fetch(APIRoot + "view_pr_detail?user_login_name=" + this.props.username + "&pr_id=" + this.props.pr_id + "&user_id=" + this.props.user_id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                }
                this.setState({
                    isLoading: false
                })
            })
            .then((responsejson) => {
                this.setState({
                    dataDetailPR: responsejson.pr_items,
                    dataDetailPRHeader: responsejson.pr_header,
                    dataApproved: responsejson.pr_logs,
                    PR: responsejson,
                    isLoading: false
                })
            })
            .catch((error) => {
                // this.setState({
                //     status: "",
                //     modalConnectionError: true,
                //     isModalVisible: true,
                //     isLoading: false
                // })
                console.log(error)
            })
    }
    handleItemDetailMaster = (pr_item_id, pr_item_account_assignment, pr_item_account_assignment_number, pr_item_material_name, pr_item_qty, pr_item_price, pr_item_currency_rate, pr_item_delivery_date, cost_center_name, gl_account_desc, pr_item_vendor_name, pr_item_vendor_address) => {
        Actions.itemHistoryDetail({ pr_number: this.props.pr_number, pr_item_account_assignment: pr_item_account_assignment, user_name: this.props.user_name, created: this.props.pr_created_time, pr_priority: this.props.pr_priority, pr_item_id: pr_item_id, pr_item_account_assignment_number: pr_item_account_assignment_number, pr_item_material_name: pr_item_material_name, pr_item_qty: pr_item_qty, pr_item_price: pr_item_price, pr_item_currency_rate: pr_item_currency_rate, pr_item_delivery_date: pr_item_delivery_date, cost_center_name: cost_center_name, gl_account_desc: gl_account_desc, pr_item_vendor_name: pr_item_vendor_name, pr_item_vendor_address: pr_item_vendor_address, user_name: this.props.username, pr_id: this.props.pr_id, user_id: this.props.user_id })
    }
    render() {
        return (
            <Container >
                <HeaderMenuComponent screen={'detailHistoryScreen'} />
                <ActivityIndicator1 size={"large"} overlayColor={"rgba(0, 0, 0, 0.5)"} animation={"fade"} visible={this.state.isLoading} textContent={"Processing..."} textStyle={{ color: '#FFF' }} />
                <StatusBar
                    backgroundColor="#6a1f08"
                    barStyle="light-content"
                />
                <View style={GlobalStyle.headerBox}>
                    <View style={GlobalStyle.titleRow}>
                        <Left>
                            <Text style={GlobalStyle.titleText}>{this.props.pr_number}</Text>
                        </Left>
                        {this.props.pr_priority === 'Medium' ?
                            <Right>
                                <View>
                                    <Badge warning>
                                        <Text style={[GlobalStyle.buttonText, { paddingTop: '2%', paddingRight: '2%', paddingLeft: '2%' }]}>Medium</Text>
                                    </Badge>
                                </View>
                            </Right> :
                            this.props.pr_priority === "High" ?
                                <Right>
                                    <View>
                                        <Badge danger>
                                            <Text style={[GlobalStyle.buttonText, { paddingTop: '2%', paddingRight: '10%', paddingLeft: '10%' }]}>High</Text>
                                        </Badge>
                                    </View>
                                </Right> :
                                this.props.pr_priority === 'Low' ?
                                    <Right>
                                        <View>
                                            <Badge style={{ backgroundColor: '#808000' }}>
                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '2%', paddingRight: '10%', paddingLeft: '10%' }]}>Low</Text>
                                            </Badge>
                                        </View>
                                    </Right> : null
                        }
                    </View>
                    <View style={GlobalStyle.subtitleRow}>
                        <Left>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[GlobalStyle.subtitleText, { fontWeight: 'bold' }]}>Name : </Text>
                                <Text style={GlobalStyle.subtitleText}>{this.props.user_name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[GlobalStyle.subtitleText, { fontWeight: 'bold' }]}>Created on : </Text>
                                <Text style={GlobalStyle.subtitleText}>{this.props.pr_created_time}</Text>
                            </View>
                        </Left>
                    </View>
                </View>
                <Tabs initialPage={0} tabBarUnderlineStyle={GlobalStyle.tabBarUnderlineRed}>
                    <Tab heading="Items" tabStyle={GlobalStyle.tabUnderlineRed} textStyle={GlobalStyle.tabTextRed} activeTabStyle={GlobalStyle.tabActiveRed} activeTextStyle={GlobalStyle.tabTextActiveRed}>
                        <ScrollView>
                            <View>
                                <Separator bordered style={{ paddingLeft: 0 }}>
                                    <ListItem>
                                        <Left>
                                            <Text style={GlobalStyle.headingText}>Total Item ({this.state.dataDetailPR.length})</Text>
                                        </Left>
                                    </ListItem>
                                </Separator>
                                <List>
                                    {this.state.dataDetailPR.map((item, index) =>
                                        <ListItem key={item.pr_item_id} onPress={() => this.handleItemDetailMaster(item.pr_item_id, item.pr_item_account_assignment, item.pr_item_account_assignment_number, item.pr_item_material_name, item.pr_item_qty, item.pr_item_price, item.pr_item_currency_rate, item.pr_item_delivery_date, item.cost_center_name, item.gl_account_desc, item.pr_item_vendor_name, item.pr_item_vendor_address)}>
                                            <Body>
                                                <View style={GlobalStyle.itemTextAlign}>
                                                    <Text style={GlobalStyle.headingText}>Item Name : </Text>
                                                    <Text style={GlobalStyle.contentText}>{item.pr_item_material_name}{"\n"}</Text>
                                                </View>

                                                <View style={GlobalStyle.itemTextAlign}>
                                                    <Text style={GlobalStyle.headingText}>Item Quantity : </Text>
                                                    <Text style={GlobalStyle.contentText}>{item.pr_item_qty}{"\n"}</Text>
                                                </View>

                                                <View style={GlobalStyle.itemTextAlign}>
                                                    <Text style={GlobalStyle.headingText}>Item Unit Price : </Text>
                                                    <Text style={GlobalStyle.contentText}>{item.pr_item_price}{"\n"}</Text>
                                                </View>

                                                <View style={GlobalStyle.itemTextAlign}>
                                                    <Text style={GlobalStyle.headingText}>Item Price : </Text>
                                                    <Text style={GlobalStyle.contentText}>{item.pr_item_price}</Text>
                                                </View>
                                            </Body>
                                            
                                    <Right style={GlobalStyle.statusTextAlign}>

                                            {item.pr_is_rejected === '1' ?
                                            <Right>
                                                <View>
                                                    <Badge warning>
                                                        <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}> APPROVED</Text>
                                                    </Badge>
                                                </View>
                                            </Right> :
                                            <Right>
                                                <View>
                                                    <Badge danger>
                                                        <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}>  REJECTED </Text>
                                                    </Badge>
                                                </View>
                                            </Right>
                                        }
                                    </Right>
                                        </ListItem>
                                    )}
                                </List>
                            </View>
                        </ScrollView>
                    </Tab>
                    <Tab heading="Detail" tabStyle={GlobalStyle.tabUnderlineRed} textStyle={GlobalStyle.tabTextRed} activeTabStyle={GlobalStyle.tabActiveRed} activeTextStyle={GlobalStyle.tabTextActiveRed}>
                        <Content style={{ padding: 15 }}>
                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Document Type : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_doc_type_desc}{"\n"}</Text>
                            </View>
                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Created By : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_created_by}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Reference : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_reference}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Purchase date : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_purchase_date}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>Purchase Purpose : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_purpose}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Transfer Date : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_transfer_date}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Cash Date : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_cash_date}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>Created On : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_created_time}{"\n"}</Text>
                            </View>

                        </Content>
                    </Tab>
                    <Tab heading="Log" tabStyle={GlobalStyle.tabUnderlineRed} textStyle={GlobalStyle.tabTextRed} activeTabStyle={GlobalStyle.tabActiveRed} activeTextStyle={GlobalStyle.tabTextActiveRed}>
                        <ScrollView>
                            <View>
                                <Separator bordered>
                                    <Text style={GlobalStyle.headingText}>Approved Total ({this.state.dataApproved.length})</Text>
                                </Separator>
                                <List>
                                    {this.state.dataApproved.map((item) =>
                                        <ListItem>
                                            <Body>
                                                <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>{item.user_name}</Text>
                                                <Text note style={GlobalStyle.subtitleText}>Remark : {item.pr_remarks}</Text>
                                                <Text note style={GlobalStyle.subtitleText}>{this.props.pr_created_time}</Text>
                                            </Body>
                                            <Right style={GlobalStyle.statusTextAlign}>
                                                {item.release_status === '1' ?
                                                    <Right>
                                                        <View>
                                                            <Badge warning>
                                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}> APPROVED</Text>
                                                            </Badge>
                                                        </View>
                                                    </Right> :
                                                    < Right >
                                                        <View>
                                                            <Badge danger>
                                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}>  REJECTED </Text>
                                                            </Badge>
                                                        </View>
                                                    </Right>
                                                }
                                            </Right>
                                        </ListItem>
                                    )}
                                </List>
                            </View>
                        </ScrollView>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}
HistoryDetail.contextTypes = {
    drawer: PropTypes.object
}