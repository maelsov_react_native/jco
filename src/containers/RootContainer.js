import React, { Component } from 'react';
import { StatusBar, View } from 'react-native';
import NavigationRouter from '../navigations/NavigationRouter'

export default class RootContainer extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="#6a1f08" />
        <NavigationRouter />
      </View>
    );
  }
}