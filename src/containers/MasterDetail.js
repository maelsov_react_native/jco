import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    Platform,
    StatusBar
} from 'react-native';
import { Container, List, ListItem, Left, Right, CheckBox, Body, Separator, Button, Footer, FooterTab, Tab, Tabs, Content, Badge, Item, Input } from 'native-base';
import GlobalStyle from '../GlobalStyle';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import { toastError, toastSuccess, toastWarning } from '../Global';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome';
import { _totalPriceSum, ifNull, _idr } from '../Global';
import ActivityIndicator from 'react-native-loading-spinner-overlay';
import HeaderMenuComponent from '../components/HeaderMenuComponent';
import { APIRoot } from '../Const'
import ActivityIndicator1 from 'react-native-loading-spinner-overlay';
import delay from '../libraries/timeoutPromise'

var checked = []
var truePRID = []

export default class MasterDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataDetailPR: [],
            PR: [],
            dataDetailPRHeader: [],
            dataApproved: [],
            checkAll: false,
            total_price: 0,
            checked: [],
            footerCheck: true,
            modal_notes: '',
            isLoading: false,
            modalStart: false,
            modalCancel: false,
        }
    }

    componentDidMount() {
        this.fetchData()
    }

    fetchData = () => {
        this.setState({
            isLoading: true
        })
        console.log(APIRoot + "view_pr_detail?user_login_name=" + this.props.username + "&pr_id=" + this.props.pr_id + "&user_id=" + this.props.user_id)
        fetch(APIRoot + "view_pr_detail?user_login_name=" + this.props.username + "&pr_id=" + this.props.pr_id + "&user_id=" + this.props.user_id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                }
                this.setState({
                    isLoading: false
                })
            })
            .then((responsejson) => {
                this.setState({
                    dataDetailPR: responsejson.pr_items,
                    dataDetailPRHeader: responsejson.pr_header,
                    dataApproved: responsejson.pr_logs,
                    PR: responsejson,
                    isLoading: false
                }, () => {
                    this.checkAllItem()
                })
            })
            .catch((error) => {
                // this.setState({
                //     status: "",
                //     modalConnectionError: true,
                //     isModalVisible: true,
                //     isLoading: false
                // })
                console.log(error)
            })
    }
    checkItem = async (param) => {
        if (this.state.checked[param] === true) {
            checked[param] = false

        }
        else {
            checked[param] = true
        }

        var isCheckAll = checked.some(check => check == false)

        if (isCheckAll == true) {
            this.setState({
                checkAll: false,
            })
        }
        else {
            this.setState({
                checkAll: true,
            })
        }

        this.setState({
            checked: checked,
        })
    }

    checkAllItem = () => {
        if (this.state.checkAll === true) {
            for (a = 0; a < this.state.dataDetailPR.length; a++) { checked[a] = false }
            this.setState({
                checked: checked,
                checkAll: false,
                footerCheck: false
            })

        }
        else {
            for (a = 0; a < this.state.dataDetailPR.length; a++) { checked[a] = true }
            this.setState({
                checked: checked,
                checkAll: true,
                footerCheck: true
            })
        }
    }

    handleModalCancel = () => {
        this.setState({
            modalConnectionError: false,
            modalCancel: true,
            modal_notes: "",
            isModalVisible: true
        })
    }

    handleModalStart = () => {
        for (var i = 0; i < checked.length; i++) {
            //Check Checkbox Bool
            if (checked[i] === true) {
                // Enter PR Item to local array
                truePRID.push(this.state.dataDetailPR[i].pr_item_id)
            }
        }
        if (truePRID.length === 0) {
            toastWarning('Please check minimal 1 PR!')
            this._hideModal()
        }
        else {
            this.setState({
                modalConnectionError: false,
                modalStart: true,
                modal_notes: "",
                isModalVisible: true
            })
        }
    }
    _hideModal = () => {
        this.setState({
            visible: false,
            isModalVisible: false,
            modalConnectionError: false,
            modalStart: false,
            modalCancel: false,
            modal_notes: ""
        })
        truePRID=[]
    }
    handleItemDetailMaster = (pr_item_id, pr_item_account_assignment, pr_item_account_assignment_number, pr_item_material_name, pr_item_qty, pr_item_price, pr_item_currency_rate, pr_item_delivery_date, cost_center_name, gl_account_desc, pr_item_vendor_name, pr_item_vendor_address) => {
        Actions.itemMasterDetail({ pr_number: this.props.pr_number, pr_item_account_assignment: pr_item_account_assignment, user_name: this.props.user_name, created: this.props.created, pr_priority: this.props.pr_priority, pr_item_id: pr_item_id, pr_item_account_assignment_number: pr_item_account_assignment_number, pr_item_material_name: pr_item_material_name, pr_item_qty: pr_item_qty, pr_item_price: pr_item_price, pr_item_currency_rate: pr_item_currency_rate, pr_item_delivery_date: pr_item_delivery_date, cost_center_name: cost_center_name, gl_account_desc: gl_account_desc, pr_item_vendor_name: pr_item_vendor_name, pr_item_vendor_address: pr_item_vendor_address, user_name: this.props.username, pr_id: this.props.pr_id, user_id: this.props.user_id })
    }
    handleApprove = () => {
        this.setState({ isModalVisible: false, modalConnectionError: false })
        // console.warn(checked)
        // console.warn(this.state.dataDetailPR)
        for (var i = 0; i < checked.length; i++) {
            //Check Checkbox Bool
            if (checked[i] === true) {
                // Enter PR Item to local array
                truePRID.push(this.state.dataDetailPR[i].pr_item_id)
            }
        }
        if (this.state.modal_notes === "") {
            toastWarning("Please input PR notes!")
        }
        else if (truePRID.length === 0) {
            toastWarning('Please check minimal 1 pr!')
        }
        else {
            this.setState({ visible: true, isModalVisible: false })
            // console.log(this.props.itemCheck)
            const formdata = new FormData();
            formdata.append('user_id', this.state.PR.user_id);
            formdata.append('pr_number', this.props.pr_number);
            formdata.append('pr_remark', this.state.modal_notes);
            formdata.append('pr_remark_value', "1");
            for (var i = 0; i < truePRID.length; i++) {
                if (truePRID !== null) formdata.append('pr_item_ids[' + i + ']', truePRID[i]);
            }

            console.log(formdata)

            fetch(APIRoot + 'approve_pr', {
                method: 'POST',
                headers: {
                    'Accept': 'multipart/form-data',
                    'Content-Type': 'multipart/form-data',
                    // 'X-Api-Key': res !== null ? res.user_token : ''
                },
                body: formdata
            })
                .then(response => {
                    console.log(response)
                    const statusCode = response.status;
                    const res = response.json();
                    return Promise.all([statusCode, res])
                })
                .then(([statusCode, result]) => {
                    delay(2000)
                    this.setState({
                        modal_notes: ""
                    })
                    this._hideModal()
                    if (statusCode === 200) {
                        toastSuccess("PR approved success!")
                        delay(1500).then(() => Actions.master())
                    }
                    else {
                        toastError("There was an error when submitting your request!")
                    }
                })
                .catch((error) => {
                    console.warn(error.message);
                    this.setState({
                        modalConnectionError: true,
                        visible: false,
                        modalCancel: false,
                        modalStart: false
                    })
                })
        }
    }
    handleReject = () => {
        this.setState({ isModalVisible: false, modalConnectionError: false })
        // console.warn(checked)
        // console.warn(this.state.dataDetailPR)

        if (truePRID.length === 0) {
            toastWarning('Please check minimal 1 pr!')
        }
        else if (this.state.modal_notes === "") {
            toastWarning("Please input PR notes!")
        }

        else {
            this.setState({ visible: true, isModalVisible: false })
            // console.log(this.props.itemCheck)
            const formdata = new FormData();
            formdata.append('user_id', this.state.PR.user_id);
            formdata.append('pr_number', this.props.pr_number);
            formdata.append('pr_remark', this.state.modal_notes);
            formdata.append('pr_remark_value', 0);
            for (var i = 0; i < truePRID.length; i++) {
                if (truePRID !== null) formdata.append('pr_item_ids[' + i + ']', truePRID[i]);
            }

            console.log(formdata)

            fetch(APIRoot + 'approve_pr', {
                method: 'POST',
                headers: {
                    'Accept': 'multipart/form-data',
                    'Content-Type': 'multipart/form-data',
                    // 'X-Api-Key': res !== null ? res.user_token : ''
                },
                body: formdata
            })
                .then(response => {
                    console.log(response)
                    const statusCode = response.status;
                    const res = response.json();
                    return Promise.all([statusCode, res])
                })
                .then(([statusCode, result]) => {
                    delay(2000)
                    this.setState({
                        modal_notes: ""
                    })
                    this._hideModal()
                    if (statusCode === 200) {
                        toastSuccess("Your PR has been rejected successfully!")
                        delay(1500).then(() => Actions.master())
                    }
                    else {
                        toastError("There was an error when submitting your request!")
                    }
                })
                .catch((error) => {
                    console.warn(error.message);
                    this.setState({
                        modalConnectionError: true,
                        visible: false,
                        modalCancel: false,
                        modalStart: false
                    })
                })
        }
    }
    render() {
        var modalContent;
        console.log(this.state.checkAll)
        if (this.state.modalStart === true) {
            modalContent = <View>
                <Text style={GlobalStyle.modalTitle}>Are you sure you want to approve this PR?</Text>
                <Item regular style={{ borderRadius: 5, marginBottom: 15 }}>
                    <Input placeholder='Notes about this PR...' value={this.state.modal_notes} onChangeText={modal_notes => this.setState({ modal_notes })} />
                </Item>
                <View style={{ flexDirection: "row", justifyContent: "center" }}>
                    <Button full onPress={() => this.handleApprove()} style={GlobalStyle.yesButton}>
                        <Text style={GlobalStyle.buttonText}>Yes</Text>
                    </Button>
                    <Button full onPress={this._hideModal} style={GlobalStyle.noButton}>
                        <Text style={GlobalStyle.buttonText}>No</Text>
                    </Button>
                </View>
            </View>;
        }
        else if (this.state.modalCancel === true) {
            modalContent = <View>
                <Text style={GlobalStyle.modalTitle}>Are you sure you want to reject this PR?</Text>
                <Item regular style={{ borderRadius: 5, marginBottom: 15 }}>
                    <Input placeholder='Notes about this PR...' value={this.state.modal_notes} onChangeText={modal_notes => this.setState({ modal_notes })} />
                </Item>
                <View style={{ flexDirection: "row", justifyContent: "center" }}>
                    <Button full onPress={() => this.handleReject()} style={GlobalStyle.yesButton}>
                        <Text style={GlobalStyle.buttonText}>Yes</Text>
                    </Button>
                    <Button full onPress={this._hideModal} style={GlobalStyle.noButton}>
                        <Text style={GlobalStyle.buttonText}>No</Text>
                    </Button>
                </View>
            </View>;
        }
        return (
            <Container>
                <HeaderMenuComponent screen="masterDetail" />
                <ActivityIndicator1 size={"large"} overlayColor={"rgba(0, 0, 0, 0.5)"} animation={"fade"} visible={this.state.isLoading} textContent={"Processing..."} textStyle={{ color: '#FFF' }} />
                <StatusBar
                    backgroundColor="#6a1f08"
                    barStyle="light-content"
                />
                <View style={GlobalStyle.headerBox}>
                    <View style={GlobalStyle.titleRow}>
                        <View>
                            <Text style={GlobalStyle.titleText}>{this.props.pr_number}</Text>
                        </View>
                        {this.props.pr_priority === 'Medium' ?
                            <Right>
                                <View>
                                    <Badge warning>
                                        <Text style={[GlobalStyle.buttonText, { paddingTop: '1%', paddingRight: '2%', paddingLeft: '2%' }]}>Medium</Text>
                                    </Badge>
                                </View>
                            </Right> :
                            this.props.pr_priority === "High" ?
                                <Right>
                                    <View>
                                        <Badge danger>
                                            <Text style={[GlobalStyle.buttonText, { paddingTop: '1%', paddingRight: '10%', paddingLeft: '10%' }]}>High</Text>
                                        </Badge>
                                    </View>
                                </Right> :
                                this.props.pr_priority === 'Low' ?
                                    <Right>
                                        <View>
                                            <Badge style={{ backgroundColor: '#808000' }}>
                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '1%', paddingRight: '10%', paddingLeft: '10%' }]}>Low</Text>
                                            </Badge>
                                        </View>
                                    </Right> : null
                        }
                    </View>
                    <View style={GlobalStyle.subtitleRow}>
                        <View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[GlobalStyle.subtitleText, { fontWeight: 'bold' }]}>Created by : </Text>
                                <Text style={GlobalStyle.subtitleText}>{this.props.user_name}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={GlobalStyle.subtitleRow}>
                        <View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[GlobalStyle.subtitleText, { fontWeight: 'bold' }]}>Created on : </Text>
                                <Text style={GlobalStyle.subtitleText}>{this.props.created}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <Tabs initialPage={0} tabBarUnderlineStyle={GlobalStyle.tabBarUnderlineRed}>
                    <Tab heading="Items" tabStyle={GlobalStyle.tabUnderlineRed} textStyle={GlobalStyle.tabTextRed} activeTabStyle={GlobalStyle.tabActiveRed} activeTextStyle={GlobalStyle.tabTextActiveRed}>
                        <ScrollView>
                            <View>
                                <Separator bordered style={{ paddingLeft: 0 }}>
                                    <ListItem>
                                        <Left>
                                            <Text style={GlobalStyle.headingText}>Total Item ({this.state.dataDetailPR.length})</Text>
                                        </Left>
                                        <Right>
                                            <Button transparent onPress={() => this.checkAllItem()} style={{ paddingRight: 10 }} >
                                                <CheckBox checked={this.state.checkAll} color='#CC8400' onPress={this.checkAllItem} style={{ borderColor: '#CC8400' }} />
                                            </Button>
                                        </Right>
                                    </ListItem>
                                </Separator>
                                <List>
                                    {this.state.dataDetailPR.map((item, index) =>
                                        <ListItem key={item.pr_item_id} onPress={() => this.handleItemDetailMaster(item.pr_item_id, item.pr_item_account_assignment, item.pr_item_account_assignment_number, item.pr_item_material_name, item.pr_item_qty, item.pr_item_price, item.pr_item_currency_rate, item.pr_item_delivery_date, item.cost_center_name, item.gl_account_desc, item.pr_item_vendor_name, item.pr_item_vendor_address)}>
                                            <Body>
                                                <View style={GlobalStyle.itemTextAlign}>
                                                    <Text style={GlobalStyle.headingText}>Item Name : </Text>
                                                    <Text style={GlobalStyle.contentText}>{item.pr_item_material_name}{"\n"}</Text>
                                                </View>

                                                <View style={GlobalStyle.itemTextAlign}>
                                                    <Text style={GlobalStyle.headingText}>Item Quantity : </Text>
                                                    <Text style={GlobalStyle.contentText}>{item.pr_item_qty}{"\n"}</Text>
                                                </View>

                                                <View style={GlobalStyle.itemTextAlign}>
                                                    <Text style={GlobalStyle.headingText}>Item Unit Price : </Text>
                                                    <Text style={GlobalStyle.contentText}>{item.pr_item_price}{"\n"}</Text>
                                                </View>

                                                <View style={GlobalStyle.itemTextAlign}>
                                                    <Text style={GlobalStyle.headingText}>Item Price : </Text>
                                                    <Text style={GlobalStyle.contentText}>{item.pr_item_price}</Text>
                                                </View>
                                            </Body>
                                            <Right>
                                                <Button transparent onPress={() => this.checkItem(index)} style={{ paddingRight: 10 }} >
                                                    <CheckBox checked={this.state.checked[index]} onPress={() => this.checkItem(index)} color='#CC8400' style={{ borderColor: '#CC8400' }} />
                                                </Button>
                                            </Right>
                                        </ListItem>
                                    )}
                                </List>
                            </View>
                        </ScrollView>
                    </Tab>
                    <Tab heading="Detail" tabStyle={GlobalStyle.tabUnderlineRed} textStyle={GlobalStyle.tabTextRed} activeTabStyle={GlobalStyle.tabActiveRed} activeTextStyle={GlobalStyle.tabTextActiveRed}>
                        <Content style={{ padding: 15 }}>
                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Document Type : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_doc_type_desc}{"\n"}</Text>
                            </View>
                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Created By : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_created_by}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Reference : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_reference}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Purchase date : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_purchase_date}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>Purchase Purpose : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_purpose}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Transfer Date : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_transfer_date}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>PR Cash Date : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_cash_date}{"\n"}</Text>
                            </View>

                            <View style={GlobalStyle.itemTextAlign}>
                                <Text style={GlobalStyle.headingText}>Created On : </Text>
                                <Text style={GlobalStyle.contentText}>{this.state.dataDetailPRHeader.pr_created_time}{"\n"}</Text>
                            </View>

                        </Content>
                    </Tab>
                    <Tab heading="Log" tabStyle={GlobalStyle.tabUnderlineRed} textStyle={GlobalStyle.tabTextRed} activeTabStyle={GlobalStyle.tabActiveRed} activeTextStyle={GlobalStyle.tabTextActiveRed}>
                        <ScrollView>
                            <View>
                                <Separator bordered>
                                    <Text style={GlobalStyle.headingText}>Approved Total ({this.state.dataApproved.length})</Text>
                                </Separator>
                                <List>
                                    {this.state.dataApproved.map((item, index) =>
                                        <ListItem key={index}>
                                            <Body>
                                                <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>{item.user_name}</Text>
                                                <Text note style={GlobalStyle.subtitleText}>Remark : {item.pr_remarks}</Text>
                                            </Body>
                                            <Right style={GlobalStyle.statusTextAlign}>
                                                {item.release_status === '1' ?
                                                    <Right>
                                                        <View>
                                                            <Badge warning>
                                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}> APPROVED</Text>
                                                            </Badge>
                                                        </View>
                                                    </Right> :
                                                    item.release_status == '0' ?
                                                        < Right >
                                                            <View>
                                                                <Badge danger>
                                                                    <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}>  REJECTED </Text>
                                                                </Badge>
                                                            </View>
                                                        </Right> : null
                                                }
                                            </Right>
                                        </ListItem>
                                    )}
                                </List>
                            </View>
                        </ScrollView>
                    </Tab>
                </Tabs>
                <View>
                    <ActivityIndicator size={"large"} overlayColor={"rgba(0, 0, 0, 0.5)"} animation={"fade"} visible={this.state.visible} textContent={"Processing..."} textStyle={{ color: '#FFF' }} />
                    <Footer>
                        <FooterTab style={GlobalStyle.footerTab}>
                            <Button style={[GlobalStyle.approveButton, { marginRight: 20 }]} small onPress={this.handleModalStart}>
                                <Text style={GlobalStyle.approveButtonText}><Icon style={{ fontSize: 14 }} name="check" /> APPROVE</Text>
                            </Button>
                            <Button style={[GlobalStyle.notSentButton, { marginLeft: 20 }]} small onPress={this.handleModalCancel}>
                                <Text style={GlobalStyle.notSentButtonText}><Icon style={{ fontSize: 14 }} name="close" /> REJECT</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </View>
                <Modal
                    avoidKeyboard={Platform.OS === "ios" ? true : false}
                    isVisible={this.state.isModalVisible}
                    backdropOpacity={0.6}
                >
                    <View style={GlobalStyle.modalStyle}>
                        {modalContent}
                    </View>
                </Modal>
            </Container>
        )
    }
}
MasterDetail.contextTypes = {
    drawer: PropTypes.object
}