import React, { Component } from 'react';
import {
    StatusBar,
    ScrollView,
} from 'react-native';
import { Container, Header, Item, Input, Icon, Button, Text, Content, Right, View, ListItem, Body, Left, Badge } from 'native-base';
import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';
import IconBack from 'react-native-vector-icons/FontAwesome';
import GlobalStyle from '../GlobalStyle';
import HeaderMenuComponent from '../components/HeaderMenuComponent';
import { APIRoot } from '../Const'
import ActivityIndicator1 from 'react-native-loading-spinner-overlay';


export default class SearchScreenHistory extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search: '',
            search_pr: [],
            isLoading: false,
            isModalVisible: false
        }
    }

    _handleSearch = () => {
        this.setState({
            search_pr: [],
            isLoading: true
        })
        console.log(APIRoot + "search_pr?" + "user_id=" + this.props.user_id + "&status=2" + "&material_name=" + this.state.search)
        fetch(APIRoot + "search_pr?" + "user_id=" + this.props.user_id + "&status=2" + "&material_name=" + this.state.search, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                }
            })
            .then((responsejson) => {
                this.setState({
                    search_pr: responsejson,
                })
            })
            .then(() => {
                this.setState({
                    isLoading: false
                })
            })
            .catch((error) => {
                this.setState({
                    status: "",
                    modalConnectionError: true,
                    isModalVisible: true,
                    isLoading: false
                })
                console.log(error)
            })
    }
    handleFormChange = (field, value) => {
        console.log(field + "=" + value)
        this.setState({
            [field]: value,
            status: ""
        })
    }
    handleDetailHistory = (pr_id, pr_number, pr_reference, pr_priority, pr_created_time, raw_pr_created_time, pr_doc_type, user_name, is_pr_read, pr_release_status, pr_purpose) => {
        Actions.historyDetail({ pr_id: pr_id, pr_number: pr_number, pr_reference: pr_reference, pr_priority: pr_priority, pr_created_time: pr_created_time, raw_pr_created_time: raw_pr_created_time, pr_doc_type: pr_doc_type, user_name: user_name, is_pr_read: is_pr_read, pr_release_status: pr_release_status, pr_purpose: pr_purpose, username: this.props.username, user_id: this.props.user_id })
    }
    handleInputChange = (field) => (value) => this.handleFormChange(field, value)
    render() {
        console.log('oke')
        console.log(this.state.search_pr)
        return (
            <Container>
                <ActivityIndicator1 size={"large"} overlayColor={"rgba(0, 0, 0, 0.5)"} animation={"fade"} visible={this.state.isLoading} textContent={"Processing..."} textStyle={{ color: '#FFF' }} />
                <Header searchBar rounded iosBarStyle="light-content" style={{ backgroundColor: "#ff9c04" }}>
                    <StatusBar
                        backgroundColor="#6a1f08"
                        barStyle="light-content"
                    />
                    <Button style={{ paddingBottom: 8, flex: 0.1 }}
                        onPress={() => {
                            setTimeout(() => { Actions.refresh({ refresh: false }) }, 500); Actions.pop();
                        }}
                        transparent>
                        <IconBack name="angle-left" style={{ color: "#6a1f08", fontSize: 28 }} />
                    </Button>
                    <Item style={{ flex: 0.9 }}>
                        <Icon name="ios-search" onPress={this._handleSearch} />
                        <Input
                            placeholder="Search"
                            value={this.state.search}
                            keyboardType="default"
                            returnKeyType='search'
                            autoFocus
                            onChangeText={this.handleInputChange('search')}
                            onSubmitEditing={this._handleSearch}
                        />
                    </Item>
                </Header>
                <ScrollView style={{ paddingBottom: '70%' }}>
                    {this.state.search_pr.map((item) => {
                        return (
                            <ListItem key={item.pr_id} onPress={() => this.handleDetailHistory(item.pr_id, item.pr_number, item.pr_reference, item.pr_priority, item.pr_created_time, item.raw_pr_created_time, item.pr_doc_type, item.user_name, item.is_pr_read, item.pr_release_status, item.pr_purpose)}>
                                <Body>
                                <View style={GlobalStyle.titleRow}>
                                    <Left>
                                        <Text style={GlobalStyle.titleText}>{item.pr_number}</Text>
                                    </Left>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Name : </Text>
                                    <Text note style={GlobalStyle.subtitleText}>{item.user_name}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Created on : </Text>
                                    <Text note style={[GlobalStyle.subtitleText, { paddingRight: '5%' }]}>{item.pr_created_time}</Text>
                                </View>
                            </Body>
                            <Right style={GlobalStyle.statusTextAlign}>
                                {item.pr_release_status === '1' ?
                                    <Right>
                                        <View>
                                            <Badge warning>
                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}> APPROVED</Text>
                                            </Badge>
                                        </View>
                                    </Right> :
                                    <Right>
                                        <View>
                                            <Badge danger>
                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}>  REJECTED </Text>
                                            </Badge>
                                        </View>
                                    </Right>
                                }
                            </Right>
                            </ListItem>
                        )

                    }
                    )}
                    {this.state.search_pr.length === 0 ?
                        <View style={[GlobalStyle.titleRow, { justifyContent: "center", paddingTop: '50%' }]}>
                            <Text style={GlobalStyle.statusText}>No Purchase Request Found</Text>
                        </View> : null
                    }
                </ScrollView>
            </Container>
        );
    }
}
SearchScreenHistory.contextTypes = {
    drawer: PropTypes.object
}