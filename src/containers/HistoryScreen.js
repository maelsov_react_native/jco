import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    StatusBar,
    RefreshControl,
    Platform
} from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import { Actions } from 'react-native-router-flux';
import GlobalStyle from '../GlobalStyle';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Header, ListItem, Body, Left, Right, Container, Badge, Button, Form, Picker, Content } from 'native-base';
import DatePicker from 'react-native-datepicker';
import PurchaseRequest from '../models/PurchaseRequest';
import delay from '../libraries/timeoutPromise'
import { _idr, replaceString } from '../Global';
import { APIRoot } from '../Const'

export default class HistoryScreen extends Component {
    constructor(props) {
        super(props);
        var today = new Date();
        var ddToday = today.getDate();
        var mmToday = today.getMonth()+1;
        var yyyyToday = today.getFullYear();
        var mmUntil = today.getMonth()-1;

        if (ddToday < 10) {
            ddToday = '0' + ddToday
        }

        if (mmToday < 10) {
            mmToday = '0' + mmToday
        }

        if (mmUntil < 1) {
            var yyyyUntil = today.getFullYear()-1;
            mmUntil = "11"
        }
        else if (mmUntil < 10) {
            var yyyyUntil = today.getFullYear();
            mmUntil = '0' + mmUntil
        }

        var end = new Date();
        var ddEnd = end.getDate();
        var mmEnd = end.getMonth()+1;
        var yyyyEnd = end.getFullYear();

        if (ddEnd < 10) {
            ddEnd = '0' + ddEnd
        }

        if (mmEnd < 10) {
            mmEnd = '0' + mmEnd
        }

        todayStartDate = '01' + '/' + mmUntil + '/' + yyyyUntil;
        today = ddToday + '/' + mmToday + '/' + yyyyToday;
        end = ddEnd + '/' + mmEnd + '/' + yyyyEnd;

        this.state = {
            historyPR: [],
            isLoading: false,
            isCache: false,
            modalFilter: false,
            isModalVisible: false,
            startDate: todayStartDate,
            endDate: end,
            minDate: todayStartDate,
            maxDate: end,
            dataPR_substitude: []
        };
    }

    onValueChangeStatus(value) {
        this.setState({
            status: value
        })
    }

    componentDidMount() {
        this.fetchData()
    }

    handleFiterModal = () => {
        this.setState({ modalFilter: true, isModalVisible: true })
    }

    handleSearchHistory = () => {
        Actions.searchHistory({ username: this.props.user_name, user_id: this.props.user_id  })
    }

    handleFiterData = (minDate, maxDate, status) => {
        this.setState({ isCache: true, isLoading: true })
        this._hideModal()

        console.log('filter data')
        PurchaseRequest.cacheAndRequestFilterHistory(replaceString('-', '/', minDate),
            replaceString('-', '/', maxDate), status, ({ data, updatedAt, isCache }) => {
                console.log(data)
                this.setState({
                    historyPR: data.purchase_requests !== null && data.purchase_requests !== undefined ? data.purchase_requests : [],
                    dataPR_substitude: data.purchase_request_substitude !== null && data.purchase_request_substitude !== undefined ? data.purchase_request_substitude : [],
                    isCache
                })
            })
        delay(2000).then(() => {
            this.setState({ isLoading: false })
        })
    }

    defaultFilter = () => {
        var today = new Date();
        var ddToday = today.getDate();
        var mmToday = today.getMonth()+1;
        var yyyyToday = today.getFullYear();
        var mmUntil = today.getMonth()-1;

        if (ddToday < 10) {
            ddToday = '0' + ddToday
        }

        if (mmToday < 10) {
            mmToday = '0' + mmToday
        }

        if (mmUntil < 1) {
            var yyyyUntil = today.getFullYear()-1;
            mmUntil = "11"
        }
        else if (mmUntil < 10) {
            var yyyyUntil = today.getFullYear();
            mmUntil = '0' + mmUntil
        }

        var end = new Date();
        var ddEnd = end.getDate();
        var mmEnd = end.getMonth()+1;
        var yyyyEnd = end.getFullYear();

        if (ddEnd < 10) {
            ddEnd = '0' + ddEnd
        }

        if (mmEnd < 10) {
            mmEnd = '0' + mmEnd
        }

        todayStartDate = '01' + '/' + mmUntil + '/' + yyyyUntil;
        today = ddToday + '/' + mmToday + '/' + yyyyToday;
        end = ddEnd + '/' + mmEnd + '/' + yyyyEnd;
        this._hideModal()
        this.fetchData()
        this.setState({
            startDate: todayStartDate,
            endDate: end,
            minDate: todayStartDate,
            maxDate: end,
            status: ""
        })
    }
    _hideModal = () => this.setState({
        isModalVisible: false,
        modalFilter: false
    })
    handleFilter = () => {
        this.setState({
            modalFilter: true,
            isModalVisible: true
        })
    }
    fetchData = () => {
        this.setState({
            isLoading: true
        })
        fetch(APIRoot + "view_pr?user_id=" + this.props.user_id + "&status=0", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
            if (response.status === 200) {
                return response.json()
            }
        })
        .then((responsejson) => {
            this.setState({
                historyPR: responsejson.purchase_requests,
                dataPR_substitude: responsejson.purchase_request_substitude,
                isLoading: false
            })
        })
        .catch((error) => {
            this.setState({
                status: "",
                modalConnectionError: true,
                isModalVisible: false,
                isLoading: false
            })
            console.log(error)
        })
    }
    handleDetailHistory = (pr_id, pr_number, pr_reference, pr_priority, pr_created_time, raw_pr_created_time, pr_doc_type, user_name, is_pr_read, pr_release_status, pr_purpose) => {
        Actions.historyDetail({ pr_id: pr_id, pr_number: pr_number, pr_reference: pr_reference, pr_priority: pr_priority, pr_created_time: pr_created_time, raw_pr_created_time: raw_pr_created_time, pr_doc_type: pr_doc_type, user_name: user_name, is_pr_read: is_pr_read, pr_release_status: pr_release_status, pr_purpose: pr_purpose , username: this.props.user_name, user_id: this.props.user_id })
    }
    render() {
        console.log('ini data' + this.props.user_id)
        console.log('ini data' + this.props.user_name)
        var headerLeft, headerBody, headerRight, modalContent
        if (this.state.modalFilter === true) {
            modalContent = <View>
                <View style={{position: 'absolute', right: -35, top: -28}}>
                    <Button transparent onPress={() => this._hideModal()} style={{paddingHorizontal: 20}}>
                        <Icon name="times-circle" style={{fontSize: 22, color: "#2E2E2E"}}/>
                    </Button>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "center" }}>
                    <Text style={[GlobalStyle.modalContent, { width: 150 }]}>Start Date : </Text>
                    <Text style={[GlobalStyle.modalContent, { width: 150 }]}>End Date : </Text>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "center" }}>
                    <DatePicker
                        style={{ width: 150 }}
                        date={this.state.startDate}
                        mode="date"
                        placeholder="Pilih Tanggal"
                        format="DD-MM-YYYY"
                        minDate={this.state.minDate}
                        maxDate={this.state.maxDate}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 0,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                        }}
                        onDateChange={(date) => { this.setState({ startDate: date }) }}
                    />
                    <DatePicker
                        style={{ width: 150 }}
                        date={this.state.endDate}
                        mode="date"
                        placeholder="Pilih Tanggal"
                        format="DD-MM-YYYY"
                        minDate={this.state.startDate}
                        maxDate={this.state.maxDate}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 0,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                        }}
                        onDateChange={(date) => { this.setState({ endDate: date }) }}
                    />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                    <Text style={[GlobalStyle.modalContent, { width: 150 }]}>By Status</Text>
                    <Form style={{ width: "100%", borderBottomColor: '#444444', borderBottomWidth: 1 }}>
                        <Picker
                            style={{ width: "100%" }}
                            iosHeader="Pilih Status"
                            mode="dropdown"
                            selectedValue={this.state.status}
                            onValueChange={this.onValueChangeStatus.bind(this)}
                            placeholder="Pilih Status"
                        >
                            <Picker.Item label="All Status" value="All Priority" />
                            <Picker.Item label="Low" value="Low" />
                            <Picker.Item label="Medium" value="Medium" />
                            <Picker.Item label="High" value="High" />
                        </Picker>
                    </Form>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "center", padding: 15 }}>
                    <Button full onPress={() => this.defaultFilter()} style={GlobalStyle.clearButton}>
                        <Text style={GlobalStyle.buttonText}>Default</Text>
                    </Button>
                    <Button full
                        onPress={() => this.handleFiterData(this.state.startDate, this.state.endDate, this.state.status)}
                        style={GlobalStyle.filterButton}>
                        <Text style={GlobalStyle.buttonText}>Filter</Text>
                    </Button>
                </View>
            </View>;
        }
        headerLeft = <Left style={{ flexDirection: "row", flex: 1, justifyContent: 'flex-start' }}>
            <Button onPress={() => this.context.drawer.open()} transparent>
                <Icon name="bars" style={{ color: "#6a1f08", fontSize: 20 }} />
            </Button>
            <Button style={{ paddingBottom: 8, flex: 0.5 }}
                onPress={() => {
                    setTimeout(() => { Actions.refresh({ refresh: false }) }, 500); Actions.pop();
                }}
                transparent>
                <Icon name="angle-left" style={{ color: "#6a1f08", fontSize: 28 }} />
            </Button>
        </Left>
        headerBody = <Body style={{ justifyContent: 'center', flex: 1 }}>
            <Text style={[GlobalStyle.titleTextHeader, { textAlign: "center" }]}>History</Text>
        </Body>
        headerRight = <Right style={{ flex: 1 }}>
            <Button transparent onPress={this.handleFilter}>
                <Icon name="filter" style={{ color: "#6a1f08", fontSize: 20 }} />
            </Button>
            <Button transparent onPress={this.handleSearchHistory}>
                <Icon name="search" style={{ color: "#6a1f08", fontSize: 20 }} />
            </Button>
        </Right>
        return (
            <Container>
                <Header iosBarStyle="light-content" style={{ backgroundColor: "#ff9c04" }}>
                    <StatusBar
                        backgroundColor="#6a1f08"
                        barStyle="light-content"
                    />
                    {headerLeft}
                    {headerBody}
                    {headerRight}
                </Header>
                <ScrollView refreshControl={
                    <RefreshControl
                        onRefresh={() => this.fetchData()}
                        refreshing={this.state.isLoading}
                    />
                }
                    scrollEventThrottle={400}>
                    {this.state.historyPR.map((item) =>
                        <ListItem key={item.pr_id} onPress={() => this.handleDetailHistory(item.pr_id, item.pr_number, item.pr_reference, item.pr_priority, item.pr_created_time, item.raw_pr_created_time, item.pr_doc_type, item.user_name, item.is_pr_read, item.pr_release_status, item.pr_purpose)}>
                            <Body>
                                <View style={GlobalStyle.titleRow}>
                                    <Left>
                                        <Text style={GlobalStyle.titleText}>{item.pr_number}</Text>
                                    </Left>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Name : </Text>
                                    <Text note style={GlobalStyle.subtitleText}>{item.user_name}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Created on : </Text>
                                    <Text note style={[GlobalStyle.subtitleText, { paddingRight: '5%' }]}>{item.pr_created_time}</Text>
                                </View>
                            </Body>
                            <Right style={GlobalStyle.statusTextAlign}>
                                {item.pr_release_status === '1' ?
                                    <Right>
                                        <View>
                                            <Badge warning>
                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}> APPROVED</Text>
                                            </Badge>
                                        </View>
                                    </Right> :
                                    <Right>
                                        <View>
                                            <Badge danger>
                                                <Text style={[GlobalStyle.buttonText, { paddingTop: '3%', paddingRight: '3%' }]}>  REJECTED </Text>
                                            </Badge>
                                        </View>
                                    </Right>
                                }
                            </Right>
                        </ListItem>
                    )}
                    {this.state.historyPR.length === 0 ?
                        <View style={[GlobalStyle.titleRow, { justifyContent: "center", paddingTop: '50%' }]}>
                            <Text style={GlobalStyle.statusText}>No Purchase Request Found</Text>
                        </View> : null
                    }
                </ScrollView>
                <Modal
                    avoidKeyboard={Platform.OS === "ios" ? true : false}
                    isVisible={this.state.isModalVisible}
                    backdropOpacity={0.6}
                >
                    <View style={GlobalStyle.modalStyle}>
                        {modalContent}
                    </View>
                </Modal>
                {this.state.historyPR.length === 0 ?
                    <View style={[GlobalStyle.titleRow, { justifyContent: "center", paddingTop: '50%' }]}>
                        <Text style={GlobalStyle.statusText}>No Purchase Request Found</Text>
                    </View> : null
                }
            </Container>
        );
    }
}
HistoryScreen.contextTypes = {
    drawer: PropTypes.object
}