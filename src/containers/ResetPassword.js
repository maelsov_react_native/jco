import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    ImageBackground,
    KeyboardAvoidingView,
    StyleSheet
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Form, Label, Item, Input, Button, Spinner} from 'native-base';
import GlobalStyle from '../GlobalStyleLogin';
import { toastWarning, toastSuccess, toastError } from '../Global'
import Icon from 'react-native-vector-icons/FontAwesome';
import { APIRoot } from '../Const'
import delay from '../libraries/timeoutPromise';

const styles = StyleSheet.create({
    imageContainer: {
        height: "20%",
        flex: 0.3,
        alignItems: "center",
        justifyContent: "center"
    },
    image: {
        flex: 1,
        width: "50%",
        height: "auto",
        resizeMode: "contain"
    },
    loginButton: {
        marginTop: 20,
        borderRadius: 6,
        backgroundColor: "#CC8400"
    },
    loginButtonText: {
        fontSize: 18,
        color: "#EEE",
        fontWeight: "bold"
    }
});

class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            modalConnectionError: false,
            isModalVisible: false,
            isLoading: false
        }
    }
    handleResetKataSandi = () => {
        if (this.state.username == "") {
            this.setState({
                isLoading: false
            })
            toastWarning("Please input username!")
        }
        else {
            this.setState({
                isLoading:true
            })
            const formdata = new FormData();
            formdata.append('user_login_name', this.state.username)
            console.warn(formdata)
            fetch(APIRoot + 'forget_password', {
                method: 'POST',
                headers: {
                    'Accept': 'multipart/form-data',
                    'Content-Type': 'multipart/form-data',
                },
                body: formdata
            })
                .then(response => {
                    const statusCode = response.status
                    // const res = response.text();
                    const res = response.json();
                    return Promise.all([statusCode, res])
                    // return res
                })
                // .then(text => console.log(text))
                .then(([statusCode, result]) => {
                    console.log(statusCode)
                    if (statusCode == 200) {
                        this.setState({
                            isLoading:false
                        })
                        toastSuccess("Reset password success!")
                        delay(1500).then(() => {
                            Actions.login()
                        })
                    }

                    else {
                        toastError("Username not registered!")
                        this.setState({
                            isLoading:false
                        })
                    }
                })
                .catch((error) => {
                    console.log(error.message);
                    toastError("Connection not available!")
                })
        }
    }
    handleFormChange = (field, value) => {
        console.log(field + "=" + value)
        this.setState({
            [field]: value
        })
    }
    handleInputChange = (field) => (value) => this.handleFormChange(field, value)
    handleLogin = () => {
        Actions.login()
    }

    render() {
        return (
            <KeyboardAvoidingView style={GlobalStyle.keyboardAvoidingStyle} keyboardVerticalOffset={-100}>
                <ImageBackground style={GlobalStyle.backgroundImage} source={require('../Images/test.jpeg')}>
                    <View style={[styles.imageContainer, { flex: 0, paddingTop: "2%" }]}>
                        <Image resizeMethod="resize" source={require("../Images/jco-logo.png")} style={styles.image} />
                    </View>
                    <View style={{ paddingTop: '15%' }}></View>
                    <View style={[GlobalStyle.backgroundCenter, { paddingBottom: '60%' }]}>
                        <View style={[GlobalStyle.whiteBackgroundOverlay, { width: 300, margin: 0 , borderRadius: 6}]}>
                            <Form style={{ padding: "6%" }}>
                                <View style={{ paddingHorizontal: 10, paddingVertical: 10 }}>
                                    <Text style={[GlobalStyle.titleText, { textAlign: "center" }]}>Forgot Password</Text>
                                </View>
                                <Item floatingLabel last style={GlobalStyle.borderFloating}>
                                    <Label style={{ color: "#2E2E2E" }}>Username</Label>
                                    <Input autoCapitalize="none" style={GlobalStyle.greyText} value={this.state.username} onChangeText={this.handleInputChange("username")} />
                                </Item>
                                <Button onPress={this.handleResetKataSandi} block style={styles.loginButton}>
                                {this.state.isLoading ? <Spinner color="#EEE" /> :<Text style={GlobalStyle.buttonText}>Submit</Text>}
                                </Button>
                            </Form>
                            <Text style={[GlobalStyle.loginScreenText, { fontWeight: "bold", marginBottom: 15 }]} onPress={this.handleLogin}><Icon style={{ fontSize: 16 }} name="arrow-left" /> Login Screen</Text>
                        </View>
                    </View>
                </ImageBackground>
            </KeyboardAvoidingView>
        );
    }
}

export default ResetPassword