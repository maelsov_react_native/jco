import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    StatusBar,
    Platform,
    Image,
    RefreshControl
} from 'react-native';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import GlobalStyle from '../GlobalStyle';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Header, ListItem, Body, Left, Right, Container, Badge, Button, Form, Picker, Content } from 'native-base';
import { _idr } from '../Global';
import { APIRoot } from '../Const';
import user from '../models/User'
import Modal from 'react-native-modal';
import DatePicker from 'react-native-datepicker';
import PurchaseRequest from '../models/PurchaseRequest';
import delay from '../libraries/timeoutPromise'
import { replaceString } from '../Global';

export default class MasterScreen extends Component {
    constructor(props) {
        super(props);
        var today = new Date();
        var ddToday = today.getDate();
        var mmToday = today.getMonth()+1;
        var yyyyToday = today.getFullYear();
        var mmUntil = today.getMonth()-1;

        if (ddToday < 10) {
            ddToday = '0' + ddToday
        }

        if (mmToday < 10) {
            mmToday = '0' + mmToday
        }

        if (mmUntil < 1) {
            var yyyyUntil = today.getFullYear()-1;
            mmUntil = "11"
        }
        else if (mmUntil < 10) {
            var yyyyUntil = today.getFullYear();
            mmUntil = '0' + mmUntil
        }

        var end = new Date();
        var ddEnd = end.getDate();
        var mmEnd = end.getMonth()+1;
        var yyyyEnd = end.getFullYear();

        if (ddEnd < 10) {
            ddEnd = '0' + ddEnd
        }

        if (mmEnd < 10) {
            mmEnd = '0' + mmEnd
        }

        todayStartDate = '01' + '/' + mmUntil + '/' + yyyyUntil;
        today = ddToday + '/' + mmToday + '/' + yyyyToday;
        end = ddEnd + '/' + mmEnd + '/' + yyyyEnd;

        this.state = {
            dataPR: [],
            data: [],
            status: "",
            isCache: false,
            dataPR_substitude: [],
            modalFilter: false,
            isModalVisible: false,
            isLoading: false,
            startDate: todayStartDate,
            endDate: end,
            minDate: todayStartDate,
            maxDate: end
        };
    }
    componentWillMount = async () => {
        await user.validateToken().then(result => {
            result = JSON.parse(result)
            this.setState({
                data: result
            })
        })
        await this.fetchData()
    }
    onValueChangeStatus(value) {
        this.setState({
            status: value
        })
    }
    handleFilter = () => {
        this.setState({
            modalFilter: true,
            isModalVisible: true
        })
    }
    handleFiterData = (minDate, maxDate, status) => {
        this.setState({ isCache: true, isLoading: true, dataPR: [] })
        this._hideModal()

        console.log('filter data')
        console.log(this.state.startDate)

        PurchaseRequest.cacheAndRequestFilterPR(replaceString('-', '/', minDate),
            replaceString('-', '/', maxDate), status, ({ data, updatedAt, isCache }) => {
                console.log(data)
                this.setState({
                    dataPR: data.purchase_requests !== null && data.purchase_requests !== undefined ? data.purchase_requests : [],
                    dataPR_substitude: data.purchase_request_substitude !== null && data.purchase_request_substitude !== undefined ? data.purchase_request_substitude : [],
                    isCache
                })
            })
        delay(2000).then(() => {
            this.setState({ isLoading: false })
        })
    }
    defaultFilter = () => {
        var today = new Date();
        var ddToday = today.getDate();
        var mmToday = today.getMonth()+1;
        var yyyyToday = today.getFullYear();
        var mmUntil = today.getMonth()-1;

        if (ddToday < 10) {
            ddToday = '0' + ddToday
        }

        if (mmToday < 10) {
            mmToday = '0' + mmToday
        }

        if (mmUntil < 1) {
            var yyyyUntil = today.getFullYear()-1;
            mmUntil = "11"
        }
        else if (mmUntil < 10) {
            var yyyyUntil = today.getFullYear();
            mmUntil = '0' + mmUntil
        }

        var end = new Date();
        var ddEnd = end.getDate();
        var mmEnd = end.getMonth()+1;
        var yyyyEnd = end.getFullYear();

        if (ddEnd < 10) {
            ddEnd = '0' + ddEnd
        }

        if (mmEnd < 10) {
            mmEnd = '0' + mmEnd
        }

        todayStartDate = '01' + '/' + mmUntil + '/' + yyyyUntil;
        today = ddToday + '/' + mmToday + '/' + yyyyToday;
        end = ddEnd + '/' + mmEnd + '/' + yyyyEnd;
        this._hideModal()
        this.fetchData()
        this.setState({
            startDate: todayStartDate,
            endDate: end,
            minDate: todayStartDate,
            maxDate: end,
            status: ""
        })
    }
    _hideModal = () => this.setState({
        isModalVisible: false,
        modalFilter: false
    })
    fetchData = () => {
        this.setState({
            isLoading: true
        })
        console.log(APIRoot + "view_pr?user_id=" + this.state.data.user_id + "&status=1")
        fetch(APIRoot + "view_pr?user_id=" + this.state.data.user_id + "&status=1", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
            if (response.status === 200) {
                return response.json()
            }
        })
        .then((responsejson) => {
            this.setState({
                dataPR: responsejson.purchase_requests,
                dataPR_substitude: responsejson.purchase_request_substitude,
                isLoading: false
            })
        })
        .catch((error) => {
            this.setState({
                modalConnectionError: true,
                isModalVisible: false,
                isLoading: false
            })
            console.log(error)
        })
    }
    handleSearch = () => {
        Actions.search({ username: this.state.data.user_name, user_id: this.state.data.user_id  })
    }
    HandleDetailMaster = (pr_number, user_name, created, pr_priority, pr_id) => {
        Actions.masterDetail({ pr_number: pr_number, user_name: user_name, created: created, pr_priority: pr_priority, pr_id: pr_id, username: this.state.data.user_name, user_id: this.state.data.user_id })
    }

    render() {
        console.log('data terbaru ')
        console.log(this.state.dataPR)
        var headerLeft, headerBody, headerRight, modalContent
        if (this.state.modalFilter === true) {
            modalContent = <View>
                <View style={{position: 'absolute', right: -35, top: -28}}>
                    <Button transparent onPress={() => this._hideModal()} style={{paddingHorizontal: 20}}>
                        <Icon name="times-circle" style={{fontSize: 22, color: "#2E2E2E"}}/>
                    </Button>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "center" }}>
                    <Text style={[GlobalStyle.modalContent, { width: 150 }]}>Start Date : </Text>
                    <Text style={[GlobalStyle.modalContent, { width: 150 }]}>End Date : </Text>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "center" }}>
                    <DatePicker
                        style={{ width: 150 }}
                        date={this.state.startDate}
                        mode="date"
                        placeholder="Pilih Tanggal"
                        format="DD-MM-YYYY"
                        minDate={this.state.minDate}
                        maxDate={this.state.maxDate}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 0,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                            // ... You can check the source to find the other keys.
                        }}
                        onDateChange={(date) => { this.setState({ startDate: date }) }}
                    />
                    <DatePicker
                        style={{ width: 150 }}
                        date={this.state.endDate}
                        mode="date"
                        placeholder="Pilih Tanggal"
                        format="DD-MM-YYYY"
                        minDate={this.state.startDate}
                        maxDate={this.state.maxDate}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 0,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                            // ... You can check the source to find the other keys.
                        }}
                        onDateChange={(date) => { this.setState({ endDate: date }) }}
                    />
                    {/* </Form> */}
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                    <Text style={[GlobalStyle.modalContent, { width: 150 }]}>By Status</Text>
                    <Form style={{ width: "100%", borderBottomColor: '#444444', borderBottomWidth: 1 }}>
                        <Picker
                            style={{ width: "100%" }}
                            iosHeader="Pilih Status"
                            mode="dropdown"
                            selectedValue={this.state.status}
                            onValueChange={this.onValueChangeStatus.bind(this)}
                            placeholder="Pilih Status"
                        >
                            <Picker.Item label="All Status" value="All Priority" />
                            <Picker.Item label="Low" value="Low" />
                            <Picker.Item label="Medium" value="Medium" />
                            <Picker.Item label="High" value="High" />
                        </Picker>
                    </Form>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "center", padding: 15 }}>
                    <Button full onPress={() => this.defaultFilter()} style={GlobalStyle.clearButton}>
                        <Text style={GlobalStyle.buttonText}>Default</Text>
                    </Button>
                    <Button full
                        onPress={() => this.handleFiterData(this.state.startDate, this.state.endDate, this.state.status)}
                        style={GlobalStyle.filterButton}>
                        <Text style={GlobalStyle.buttonText}>Filter</Text>
                    </Button>
                </View>
            </View>;
        }
        headerLeft = <Left style={{ flexDirection: "row", flex: 1, justifyContent: 'flex-start' }}>
            <Button onPress={() => this.context.drawer.open()} transparent>
                <Icon name="bars" style={{ color: "#6a1f08", fontSize: 20 }} />
            </Button>
        </Left>
        headerBody = <Body style={{ justifyContent: 'center', flex: 1 }}>
            <Image resizeMethod="resize" source={require("../Images/jco-text.png")}
                style={{ width: 110, height: 30, resizeMode: "contain" }} />
        </Body>
        headerRight = <Right style={{ flex: 1 }}>
            <Button transparent onPress={this.handleFilter}>
                <Icon name="filter" style={{ color: "#6a1f08", fontSize: 20 }} />
            </Button>
            <Button transparent onPress={this.handleSearch}>
                <Icon name="search" style={{ color: "#6a1f08", fontSize: 20 }} />
            </Button>
        </Right>
        return (
            <Container>
                <Header iosBarStyle="light-content" style={{ backgroundColor: "#ff9c04", elevation: 0 }}>
                    <StatusBar
                        backgroundColor="#6a1f08"
                        barStyle="light-content"
                    />
                    {headerLeft}
                    {headerBody}
                    {headerRight}
                </Header>
                <ScrollView refreshControl={
                    <RefreshControl
                        onRefresh={() => this.fetchData()}
                        refreshing={this.state.isLoading}
                    />
                }
                    scrollEventThrottle={400}>
                    {this.state.dataPR.map((item) =>
                        <ListItem key={item.pr_id} onPress={() => this.HandleDetailMaster(item.pr_number, item.user_name, item.pr_created_time, item.pr_priority, item.pr_id)}>
                            <ScrollView>
                                <View style={GlobalStyle.titleRow}>
                                    <Left>
                                        <Text style={GlobalStyle.titleText}>{item.pr_number}</Text>
                                    </Left>
                                    {item.pr_priority == 'High' ?
                                        <Right style={{paddingRight:'3%' }}>
                                            <View style={{ width: '100%' }}>
                                                <Badge danger style={{width: '110%'}}>
                                                    <Text style={[GlobalStyle.buttonText, { paddingTop: '5%', textAlign: 'center' }]}>High</Text>
                                                </Badge>
                                            </View>
                                        </Right>
                                        : item.pr_priority == "Low" ?
                                            <Right style={{paddingRight:'3%' }}>
                                                <View style={{ width: '100%' }}>
                                                    <Badge style={{ backgroundColor: '#808000', width: '110%' }} >
                                                        <Text style={[GlobalStyle.buttonText, { paddingTop: '5%', textAlign: 'center' }]}>Low</Text>
                                                    </Badge>
                                                </View>
                                            </Right>
                                            : item.pr_priority == "Medium" ?
                                                <Right style={{paddingRight:'3%' }}>
                                                    <View style={{ width: '100%'}}>
                                                        <Badge warning style={{width: '110%'}}>
                                                            <Text style={[GlobalStyle.buttonText, { paddingTop: '4%', textAlign: 'center' }]}>Medium</Text>
                                                        </Badge>
                                                    </View>
                                                </Right> : null
                                    }
                                </View>
                                <Body>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Purpose : </Text>
                                        <Text note style={[GlobalStyle.subtitleText, { paddingRight: '5%' }]}>{item.pr_purpose}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Created by : </Text>
                                        <Text note style={[GlobalStyle.subtitleText, { paddingRight: '5%' }]}>{item.user_name}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text note style={[GlobalStyle.subtitleText, { fontWeight: "600" }]}>Created on : </Text>
                                        <Text note style={[GlobalStyle.subtitleText, { paddingRight: '5%' }]}>{item.pr_created_time}</Text>
                                    </View>
                                </Body>
                            </ScrollView>
                        </ListItem>
                    )}
                    {this.state.dataPR.length === 0 ?
                        <View style={[GlobalStyle.titleRow, { justifyContent: "center", paddingTop: '50%' }]}>
                            <Text style={GlobalStyle.statusText}>No Purchase Request Found</Text>
                        </View> : null
                    }
                </ScrollView>
                <Modal
                    avoidKeyboard={Platform.OS === "ios" ? true : false}
                    isVisible={this.state.isModalVisible}
                    backdropOpacity={0.6}
                >
                    <View style={GlobalStyle.modalStyle}>
                        {modalContent}
                    </View>
                </Modal>
            </Container>
        );
    }
}
MasterScreen.contextTypes = {
    drawer: PropTypes.object
}